﻿using System.Text.RegularExpressions;

namespace Hoerspur
{
    using UnityEngine;
    using Hoerspur.GeoJson;
    using Hoerspur.Triggers;
    using System.Collections.Generic;
    using Hoerspur.Utils;
    using UnityEngine.Audio;
    using Hoerspur.Location;

    public class HoerspurManager : SingletonSimple<HoerspurManager>
    {
        // most of the fields are not needed during runtime
        // they are inu this class because that allows us to serialize them
        // and keep them in between editor sessions
#if UNITY_EDITOR
        public TextAsset geoJson;
        public string mapBoxApiToken = "pk.eyJ1Ijoic21ucGhsciIsImEiOiJjamEyamc4aDI4cjZvMndxcTU0Z2wyMm52In0.GVFvbDXZTrB-50FYnol27g";
        public MapScale mapScale = MapScale.One;
        public MapStyle mapStyle = MapStyle.StreetsV10;
#endif

        public Vector2 center;

        private List<ATrigger> currentTriggers;

        private void Awake()
        {
            currentTriggers = new List<ATrigger>();
            LocationManager.Instance.onLocationDataAvailable.AddListener(OnNewLocation);
        }

        public void OnNewLocation(LocationData data)
        {
            var triggers = new List<ATrigger>(GetComponentsInChildren<ATrigger>());
            foreach (var trigger in triggers)
            {
                if (trigger.ContainsWorldPoint(data.worldPosition) && !currentTriggers.Contains(trigger))
                {
                    //enter trigger
                    currentTriggers.Add(trigger);
                    trigger.Enter();
                }
                else if (currentTriggers.Contains(trigger) && !trigger.ContainsWorldPoint(data.worldPosition))
                {
                    // exit trigger
                    currentTriggers.Remove(trigger);
                    trigger.Exit();
                }
            }
        }
    }

    public enum MapScale
    {
        Quarter,
        Half,
        One,
        Two,
        Four,
        Eight,
        Sixteen,
        Thirtytwo
    }

    public enum MapStyle
    {
        StreetsV10,
        OutdoorsV10,
        LightV9,
        DarkV9,
        SatelliteV9,
        SatelliteStreetsV10,
        NavigationPreviewDayV2,
        NavigationPreviewNightV2,
        NavigationGuidanceDayV2,
        NavigationGuidanceNightV2
    }

    public static class MapEnumsExtensions
    {
        public static float Value(this MapScale mapScale)
        {
            switch (mapScale)
            {
                case MapScale.Quarter:
                    return .25f;
                case MapScale.Half:
                    return .5f;
                case MapScale.One:
                    return 1f;
                case MapScale.Two:
                    return 2f;
                case MapScale.Four:
                    return 4f;
                case MapScale.Eight:
                    return 8f;
                case MapScale.Sixteen:
                    return 16f;
                case MapScale.Thirtytwo:
                    return 32f;
                default:
                    return 1f;
            }
        }

        public static string Value(this MapStyle mapStyle)
        {
            var result = Regex.Replace(mapStyle.ToString(), "(?<=.)([A-Z])", "-$0").ToLower();
            return result;
        }
    }
}

