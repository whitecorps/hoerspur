﻿namespace Hoerspur.UI
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class Collapsible : MonoBehaviour
    {
        public bool startCollapsed = true;
        public GameObject[] collapsibleElements;

        private bool isCollapsed;

        // Use this for initialization
        void Start()
        {
            SetCollapsed(startCollapsed);
        }

        public void SetCollapsed(bool collapsed)
        {
            isCollapsed = collapsed;
            //int prefHeight = -1;
            //if (collapsed)
            //{
            //    prefHeight = 0;
            //}
            foreach (var element in collapsibleElements)
            {
                //element.preferredHeight = prefHeight;
                element.SetActive(!collapsed);
                Debug.Log("setting");
            }
            LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent.GetComponent<RectTransform>());
        }

        public void ToggleCollapsed()
        {
            SetCollapsed(!isCollapsed);
        }
    }
}
