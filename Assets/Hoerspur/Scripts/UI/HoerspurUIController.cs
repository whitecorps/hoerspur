﻿namespace Hoerspur.UI
{
    using Hoerspur.Audio;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class HoerspurUIController : MonoBehaviour
    {
        [Header("Hoerspur Components")]
        public AudioManager audioManager;

        [Header("Audio Controls")]
        [Tooltip("These buttons' state is toggled depending on wether there is audio currently playing. If there is a clip, these buttons are active, otherwise they are deactivated.")]
        public List<Button> audioControlButtons;
        public RectTransform playButtonContent;
        public RectTransform pauseButtonContent;

        [Header("Log")]
        public RectTransform logPanel;
        public RectTransform logEntryContainer;
        public LogEntryController logEntryPrefab;
        public int logEntryNewSiblingIndexPosition = 2;

        private bool supportsPlayPauseButtonContentToggle;

        private void Awake()
        {
            supportsPlayPauseButtonContentToggle = (playButtonContent != null && pauseButtonContent != null && playButtonContent.parent == pauseButtonContent.parent);
        }

        void Start()
        {
            if (!audioManager)
            {
                Debug.LogError("No AudioManager set.", this);
            }
            else
            {
                StartCoroutine(AttachLogToAudioManager());
            }

            logPanel.gameObject.SetActive(false);
            UpdateButtons();
        }

        private void OnDestroy()
        {
            if (audioManager && audioManager.onNewAudioLog != null)
            {
                audioManager.onNewAudioLog.RemoveListener(AddLogEntry);
            }
        }

        private void Update()
        {
            UpdateButtons();
        }

        public void ToggleLogPanel()
        {
            var active = !logPanel.gameObject.activeSelf;
            if (active)
            {
                var scrollRect = logPanel.GetComponent<ScrollRect>();
                if (scrollRect)
                {
                    scrollRect.verticalNormalizedPosition = 1f;
                }
            }
            logPanel.gameObject.SetActive(active);
        }

        public void TogglePlayPause()
        {
            audioManager.TogglePause();
        }

        public void Restart()
        {
            audioManager.Restart();
        }

        public void Skip()
        {
            audioManager.Skip();
        }

        private void UpdateButtons()
        {
            if (audioManager.HasCurrentClip)
            {
                SetButtonsEnabled(true);
                if (audioManager.Paused)
                {
                    SetPlayPauseButtonState(true);
                }
                else
                {
                    SetPlayPauseButtonState(false);
                }
            }
            else
            {
                SetButtonsEnabled(false);
            }
        }

        private void SetPlayPauseButtonState(bool play)
        {

            if (supportsPlayPauseButtonContentToggle)
            {
                playButtonContent.gameObject.SetActive(play);
                pauseButtonContent.gameObject.SetActive(!play);
            }
        }

        private void SetButtonsEnabled(bool enabled)
        {
            foreach (var btn in audioControlButtons)
            {
                btn.interactable = enabled;
            }
        }

        private IEnumerator AttachLogToAudioManager()
        {
            // wait for next frame
            // this makes sure all references in audioManager are not null, especially the onNewAudioLog event
            yield return null;
            BuildLog();
            audioManager.onNewAudioLog.AddListener(AddLogEntry);
        }

        private void BuildLog()
        {
            // destroy all log children first
            var logEntryChildren = logEntryContainer.GetComponentsInChildren<LogEntryController>();
            foreach (var child in logEntryChildren)
            {
                Destroy(child.gameObject);
            }

            // add log entries
            var log = audioManager.AudioLog;
            foreach (var logEntry in log)
            {
                AddLogEntry(logEntry);
            }
        }

        private void AddLogEntry(AudioLogEntry logEntry)
        {
            // create log entry prefab instance
            var newLogEntry = Instantiate(logEntryPrefab, logEntryContainer);
            // set logentry and audiomanager (audiomanager is needed for replay button)
            newLogEntry.SetAudioLogEntry(logEntry, audioManager);
            // set sibling index, so newest entries are displayed on top
            newLogEntry.transform.SetSiblingIndex(logEntryNewSiblingIndexPosition);
        }
    }
}