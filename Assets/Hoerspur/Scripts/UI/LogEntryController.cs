﻿namespace Hoerspur.UI
{
    using Hoerspur.Audio;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class LogEntryController : MonoBehaviour
    {
        public Text dateTime;
        public string dateTimeFormat = "dd.MM.yyyy - HH:mm";
        public Text content;
        public Button replayButton;
        
        private AudioLogEntry logEntry;
        private AudioManager audioManager;
        
        public void SetAudioLogEntry(AudioLogEntry audioLogEntry, AudioManager audioManager)
        {
            logEntry = audioLogEntry;
            dateTime.text = logEntry.dateTime.ToString(dateTimeFormat);
            content.text = audioLogEntry.info.text;
            replayButton.onClick.AddListener(Replay);

            if (!audioManager)
                Debug.LogError("AudioManager is null", this);
            else
                this.audioManager = audioManager;
        }

        private void OnDestroy()
        {
            if (replayButton && replayButton.onClick != null)
                replayButton.onClick.RemoveListener(Replay);
        }

        public void Replay()
        {
            if (!audioManager)
                Debug.LogError("AudioManager is null", this);
            else
            {
                // we clone the AudioClipInfo
                var info = logEntry.info.Clone();
                // we dont want to add this to the log, as this would create confusing double entries
                info.addToLog = false;
                // we also want to play this immediately, not some time in the future..
                info.priority = AudioClipPriority.QueueImmediate;
                // we add the cloned info to the audio manager
                audioManager.Add(info);
            }
        }
    }
}