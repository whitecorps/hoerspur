﻿namespace Hoerspur.GeoJson
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    
    public abstract class Geometry
    {
        public string type;
    }

    [System.Serializable]
    public class Coordinates : Geometry
    {
        public List<Vector2> coordinates;

        public Coordinates()
        {
            coordinates = new List<Vector2>();
        }
    }
}