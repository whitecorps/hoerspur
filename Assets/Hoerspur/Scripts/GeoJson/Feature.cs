﻿namespace Hoerspur.GeoJson
{
    using Hoerspur.Utils;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class Feature
    {
        public string id;
        public Coordinates geometry;
        public Dictionary<string, string> properties;

        public Bounds GetBounds()
        {
            return GeoLocationHelper.GetBounds(geometry.coordinates);
        }
    }
}