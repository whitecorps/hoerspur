﻿namespace Hoerspur.GeoJson
{
    using SimpleJSON;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using System;

    [System.Serializable]
    public class FeatureCollection : IEnumerable<Feature>, ICollection<Feature>
    {
        [SerializeField]
        private List<Feature> features;

        public List<Feature> Features { get { return features; } }

        bool ICollection<Feature>.IsReadOnly { get { return (features as ICollection<Feature>).IsReadOnly; } }

        public int Count { get { return features.Count; } }

        //public bool IsReadOnly { get { return features.IsRead} }

        protected FeatureCollection()
        {
            features = new List<Feature>();
        }

        public Bounds GetBounds()
        {
            if (features.Count == 0)
                //throw new UnityException("FeatureCollection has no geometry");
                return new Bounds();

            var result = features[0].GetBounds();
            for (int i = 1; i < features.Count; i++)
            {
                result.Encapsulate(features[i].GetBounds());
            }
            return result;
        }

        #region Static Methods
        public static FeatureCollection JsonToFeatureCollection(TextAsset json)
        {
            FeatureCollection result = new FeatureCollection();
            if (json == null)
            {
                Debug.LogWarning("JSON file missing");
                return result;
            }

            var n = JSON.Parse(json.text);
            foreach (var feature in n["features"])
            {
                Feature f = new Feature();
                // Get geometry
                // var geometryType = feature.Value["geometry"]["type"].Value;
                f.geometry = GetGeometry(feature.Value["geometry"]);
                f.properties = GetProperties(feature.Value["properties"]);
                f.id = GetID(feature.Value, f.properties);
                if (f.id == null || f.id == "")
                {
                    Debug.LogWarning("Feature without 'id' or 'name' found, ignoring");
                    continue;
                }
                result.features.Add(f);
            }

            return result;
        }

        static Coordinates GetGeometry(JSONNode geometryNode)
        {
            Coordinates result = new Coordinates();
            result.type = geometryNode["type"].Value;

            var array = geometryNode["coordinates"].AsArray;
            switch (result.type)
            {
                case "Point":
                    result.coordinates.Add(array.ReadVector2());
                    break;
                case "Polygon":
                    foreach (var floatPair in array[0].AsArray)
                    {
                        result.coordinates.Add(floatPair.Value.ReadVector2());
                    }
                    // remove last entry if equal to first
                    if (result.coordinates[0].Equals(result.coordinates[result.coordinates.Count - 1]))
                    {
                        result.coordinates.RemoveAt(result.coordinates.Count - 1);
                    }
                    break;
                default:
                    Debug.LogWarning("Unsupported geometry type");
                    break;
            }
            return result;
        }

        static Dictionary<string, string> GetProperties(JSONNode propertiesNode)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (var property in propertiesNode)
            {
                result.Add(property.Key, property.Value.Value);
            }
            return result;
        }

        static string GetID(JSONNode feature, Dictionary<string, string> properties)
        {
            string result = feature["id"].Value;
            if (result == "")
            {
                if (!properties.TryGetValue("id", out result))
                {
                    properties.TryGetValue("name", out result);
                }
            }
            return result;
        }
        #endregion

        #region IEnumerable interface implementation
        public IEnumerator<Feature> GetEnumerator()
        {
            return features.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return features.GetEnumerator();
        }
        #endregion

        #region ICollection interface implementation
        public void Add(Feature item)
        {
            features.Add(item);
        }

        public void Clear()
        {
            features.Clear();
        }

        public bool Contains(Feature item)
        {
            return features.Contains(item);
        }

        public void CopyTo(Feature[] array, int arrayIndex)
        {
            features.CopyTo(array, arrayIndex);
        }

        public bool Remove(Feature item)
        {
            return features.Remove(item);
        }
        #endregion
    }
}