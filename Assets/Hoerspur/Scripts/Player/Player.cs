﻿namespace Hoerspur
{
    using UnityEngine;

    [SelectionBase]
    public class Player : MonoBehaviour
    {

        public bool useCompassForHeading = true;

        private void Start()
        {
            if (useCompassForHeading)
                Input.compass.enabled = true;
        }

        private void Update()
        {
            if (useCompassForHeading && Input.compass.enabled)
            {
                float heading;
                if (Input.location.status == LocationServiceStatus.Running)
                    heading = Input.compass.trueHeading;
                else
                    heading = Input.compass.magneticHeading;

                var angles = transform.eulerAngles;
                angles.z = Input.compass.magneticHeading;
                transform.eulerAngles = angles;
            }
        }
    }
}