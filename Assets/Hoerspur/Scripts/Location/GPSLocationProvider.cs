﻿namespace Hoerspur.Location
{
    using Hoerspur;
    using Hoerspur.Utils;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class GPSLocationProvider : ALocationProvider
    {
        private const float DefaultDesiredAccuracyInMeters = 5f;
        private const float DefaultUpdateDistanceInMeters = 5f;

        private LocationProviderState currentState = LocationProviderState.NotReady;
        private double lastTimestamp = 0;
        private LocationData lastData;

        private float desiredAccuracyInMeters;
        private float updateDistanceInMeters;

        public override LocationProviderState CurrentState { get { return currentState; } }

        public GPSLocationProvider(float desiredAccuracyInMeters = DefaultDesiredAccuracyInMeters, float updateDistanceInMeters = DefaultUpdateDistanceInMeters) : base()
        {
            this.desiredAccuracyInMeters = desiredAccuracyInMeters;
            this.updateDistanceInMeters = updateDistanceInMeters;
        }

        public override LocationData GetCurrentLocation()
        {
            UpdateLocation();
            return lastData;
        }

        public override IEnumerator Initialize()
        {
            yield return new WaitForSecondsRealtime(2);
            Debug.Log("LocationProvider initializing");
            SetState(LocationProviderState.Initializing);
            // location service enabled by user?
            if (!Input.location.isEnabledByUser)
            {
                if (onError != null)
                    onError.Invoke("GPS is not enabled");
                SetState(LocationProviderState.Error);
                yield break;
            }

            // start location service
            Debug.Log("start 1: " + Input.location.status);
            Input.location.Start(desiredAccuracyInMeters, updateDistanceInMeters);
            Debug.Log("start 2: " + Input.location.status);
            yield return new WaitForSecondsRealtime(2);
            Debug.Log("start 3: " + Input.location.status);

            // wait for service to initialize
            int timeout = 20;
            while (Input.location.status == LocationServiceStatus.Initializing && timeout > 0)
            {
                Debug.Log("tineout: " + timeout);
                yield return new WaitForSecondsRealtime(1);
                timeout--;
            }

            // init timed out
            if (timeout <= 0)
            {
                if (onError != null)
                    onError.Invoke("GPS initialization timed out");
                SetState(LocationProviderState.Error);
                yield break;
            }

            if (Input.location.status == LocationServiceStatus.Failed)
            {
                // TODO: display warning, could not enable gps, unknown error
                if (onError != null)
                    onError.Invoke("GPS unknown error");
                SetState(LocationProviderState.Error);
                yield break;
            }

            SetState(LocationProviderState.Ready);
        }

        public override IEnumerator Destroy()
        {
            Input.location.Stop();
            yield return null;
        }

        public override void Update()
        {
            if (currentState == LocationProviderState.Ready)
                UpdateLocation();
        }

        private void SetState(LocationProviderState newState)
        {
            currentState = newState;
            if (onStateChange != null)
                onStateChange.Invoke(newState);
        }

        private void UpdateLocation()
        {
            var locLastData = Input.location.lastData;
            if (locLastData.timestamp > lastTimestamp)
            {
                lastTimestamp = locLastData.timestamp;

                var geoPosition = new Vector2(locLastData.longitude, locLastData.latitude);
                var geoRef = HoerspurManager.Instance.center;
                var worldRef = WebMercator.LatLonToMeters(geoRef.x, geoRef.y);
                var worldPosition = WebMercator.LatLonToMeters(geoPosition.y, geoPosition.x) - worldRef;

                lastData = new LocationData()
                {
                    geoPosition = geoPosition,
                    worldPosition = worldPosition,
                    accuracy = new Vector2(locLastData.horizontalAccuracy, locLastData.verticalAccuracy),
                    timestamp = lastTimestamp
                };

                if (onLocationDataAvailable != null)
                {
                    onLocationDataAvailable.Invoke(lastData);
                }
            }
        }
    }
}

