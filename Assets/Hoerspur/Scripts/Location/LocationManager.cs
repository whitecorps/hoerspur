﻿namespace Hoerspur.Location
{
    using Hoerspur.Location.Events;
    using UnityEngine;

    public class LocationManager : SingletonSimple<LocationManager>
    {
        private const float UpdateInterval = 1;

        public LocationProviderType defaultLocationProvider;
        public LocationDataAvailableEvent onLocationDataAvailable;
        public StateChangeEvent onStateChange;
        public ErrorEvent onError;
        public Player player;
        
        private ALocationProvider locationProvider;

        public enum LocationProviderType
        {
            GPS,
            Debug
        }

        private void Awake()
        {
            //onLocationDataAvailable = new LocationDataAvailableEvent();
            //onError = new ErrorEvent();
        }

        private void Start()
        {
            SetLocationProvider(defaultLocationProvider);
        }

        private void Update()
        {
            if (locationProvider != null)
            {
                locationProvider.Update();
            }
        }

        public void SetLocationProvider(LocationProviderType provider)
        {
            // destroy old provider if necessary
            if (locationProvider != null)
            {
                StartCoroutine(locationProvider.Destroy());
            }

            // create new provider
            switch (provider)
            {
                case LocationProviderType.GPS:
                    locationProvider = new GPSLocationProvider();
                    break;
                case LocationProviderType.Debug:
                    locationProvider = new PlayerControlLocationProvider(player.transform.position);
                    break;
            }

            // attach event handlers
            locationProvider.onError.AddListener(LocationProvider_Error);
            locationProvider.onLocationDataAvailable.AddListener(LocationProvider_LocationDataAvailable);
            locationProvider.onStateChange.AddListener(LocationProvider_StateChange);
            // init new provider
            StartCoroutine(locationProvider.Initialize());
        }

        private void LocationProvider_Error(string message)
        {
            // TODO: better error message handling
            Debug.Log(message, this);

            if (onError != null)
            {
                onError.Invoke(message);
            }
        }

        private void LocationProvider_LocationDataAvailable(LocationData data)
        {
            // TODO: triggers and stuff
            if (player != null)
            {
                player.transform.position = data.worldPosition;
            }

            if (onLocationDataAvailable != null)
            {
                onLocationDataAvailable.Invoke(data);
            }
        }

        private void LocationProvider_StateChange(LocationProviderState newState)
        {
            // TODO state handling:
            // new event in manager, hook up UI
            switch (newState)
            {
                case LocationProviderState.NotReady:
                    break;
                case LocationProviderState.Initializing:
                    break;
                case LocationProviderState.Ready:
                    break;
                case LocationProviderState.Error:
                    break;
            }

            if (onStateChange != null)
            {
                onStateChange.Invoke(newState);
            }
        }
    }
}
