﻿namespace Hoerspur.Location
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class PlayerControlLocationProvider : ALocationProvider
    {
        const float Speed = 10f;

        private LocationData current;
        private Vector2 currentPosition;

        public override LocationProviderState CurrentState { get { return LocationProviderState.Ready; } }

        public PlayerControlLocationProvider(Vector2 initialPosition) : base()
        {
            currentPosition = initialPosition;
        }

        public override LocationData GetCurrentLocation()
        {
            System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
            int timestamp = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
            return new LocationData()
            {
                geoPosition = Vector2.zero,
                worldPosition = currentPosition,
                accuracy = new Vector2(0, 0),
                timestamp = timestamp
            };
        }

        public override void Update()
        {
            // TODO: allow other inputs, eg gui buttons for mobile
            var input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * Time.deltaTime * Speed;
            currentPosition += input;
            if (onLocationDataAvailable != null)
            {
                onLocationDataAvailable.Invoke(GetCurrentLocation());
            }
        }
    }
}
