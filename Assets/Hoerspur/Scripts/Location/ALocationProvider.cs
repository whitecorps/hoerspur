﻿namespace Hoerspur.Location
{
    using Hoerspur.Location.Events;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public enum LocationProviderState
    {
        NotReady,
        Initializing,
        Ready,
        Error
    }

    public abstract class ALocationProvider
    {
        public StateChangeEvent onStateChange;
        public LocationDataAvailableEvent onLocationDataAvailable;
        public ErrorEvent onError;

        public ALocationProvider()
        {
            onStateChange = new StateChangeEvent();
            onLocationDataAvailable = new LocationDataAvailableEvent();
            onError = new ErrorEvent();
        }

        public abstract LocationProviderState CurrentState { get; }

        public abstract LocationData GetCurrentLocation();
        public abstract void Update();
        public virtual IEnumerator Initialize() { yield return null; }
        public virtual IEnumerator Destroy() { yield return null; }
    }
}
