﻿namespace Hoerspur.Location.Events
{
    using UnityEngine.Events;

    [System.Serializable]
    public class StateChangeEvent : UnityEvent<LocationProviderState> { }
    [System.Serializable]
    public class LocationDataAvailableEvent : UnityEvent<LocationData> { }
    [System.Serializable]
    public class ErrorEvent : UnityEvent<string> { }
}