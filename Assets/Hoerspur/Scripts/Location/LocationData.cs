﻿namespace Hoerspur.Location
{
    using UnityEngine;

    public struct LocationData
    {
        public Vector2 geoPosition;
        public Vector2 worldPosition;
        public Vector2 accuracy;
        public double timestamp;
    }
}
