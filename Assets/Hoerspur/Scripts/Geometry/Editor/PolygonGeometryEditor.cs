﻿namespace Hoerspur.Geometry
{
    using Hoerspur.Triggers;
    using Hoerspur.Utils;
    using System;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(PolygonGeometry))]
    public class PolygonGeometryEditor : AGeometryEditor
    {
        private const float HandleSizeMultiplier = 0.1f;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            // TODO: remove if nothing is added here
        }

        private void OnSceneGUI()
        {
            serializedObject.Update();
            var localCoordinatesProp = serializedObject.FindProperty("localCoordinates");

            if (localCoordinatesProp.arraySize == 0)
                return;

            var objectPosition = ((PolygonGeometry)serializedObject.targetObject).transform.position;

            // move/delete coords
            Handles.color = Handles.xAxisColor;
            for (int i = 0; i < localCoordinatesProp.arraySize; i++)
            {
                var localPosition = localCoordinatesProp.GetArrayElementAtIndex(i).vector2Value;
                var worldPosition = localPosition.AsVector3() + objectPosition;
                var handleSize = HandleUtility.GetHandleSize(worldPosition) * HandleSizeMultiplier;
                var result = Hoerspur.Handles.CustomHandles.FreeMoveButton(worldPosition, handleSize, Handles.DotHandleCap);
                //Handles.Label(worldPosition, i.ToString());
                if (result.rightClicked)
                {
                    localCoordinatesProp.DeleteArrayElementAtIndex(i);
                    Event.current.Use();
                    break;
                }
                else if (result.dragged)
                {
                    localCoordinatesProp.GetArrayElementAtIndex(i).vector2Value = result.position - objectPosition;
                }
            }

            // add coord
            Handles.color = Handles.yAxisColor;
            for (int i = 0; i < localCoordinatesProp.arraySize; i++)
            {
                var localPosition1 = localCoordinatesProp.GetArrayElementAtIndex(i).vector2Value;
                var localPosition2 = localCoordinatesProp.GetArrayElementAtIndex((i + 1) % localCoordinatesProp.arraySize).vector2Value;
                var localPosition = (localPosition1 + localPosition2) / 2f;
                var worldPosition = localPosition.AsVector3() + objectPosition;
                var handleSize = HandleUtility.GetHandleSize(worldPosition) * HandleSizeMultiplier * 0.5f;
                var rect = new Rect(); rect.width = rect.height = handleSize; rect.center = localPosition;
                if (Handles.Button(worldPosition, Quaternion.identity, handleSize, handleSize, Handles.DotHandleCap))
                {
                    int idx = (i + 1) % localCoordinatesProp.arraySize;
                    localCoordinatesProp.InsertArrayElementAtIndex(idx);
                    localCoordinatesProp.GetArrayElementAtIndex(idx).vector2Value = localPosition;
                    break;
                }
            }

            if (serializedObject.ApplyModifiedProperties())
            {
                var polyEditors = Resources.FindObjectsOfTypeAll<PolygonTriggerEditor>();
                if (polyEditors.Length > 0)
                {
                    polyEditors[0].UpdateMesh();
                }
            }

            // clicks inside polygon -> dont deselect gameobject
            //if (GeoLocationHelper.PolygonContainsPoint(geometry.WorldCoordinates, EditorHelper.MouseToWorldPosition()) &&
            //    Event.current.type == EventType.Layout)
            //{
            //    HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            //}
        }

        protected override void AddTrigger()
        {
            Undo.AddComponent<PolygonTrigger>(geometry.gameObject);
        }

        [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected | GizmoType.Active)]
        static void DrawPolyGeometryGizmo(PolygonGeometry geometry, GizmoType gizmoType)
        {
            bool active = ((gizmoType & GizmoType.Active) != 0);
            var coords = geometry.WorldCoordinates;
            Gizmos.color = active ? EditorHelper.GizmoLineColorActive : EditorHelper.GizmoLineColor;
            for (int i = 0; i < coords.Count - 1; i++)
            {
                Gizmos.DrawLine(coords[i], coords[i + 1]);
            }
            if (coords.Count > 2)
            {
                Gizmos.DrawLine(coords[coords.Count - 1], coords[0]);
            }
        }
    }
}