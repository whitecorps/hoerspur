﻿namespace Hoerspur.Geometry
{
    using Hoerspur.Geometry;
    using Hoerspur.Triggers;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(AGeometry), true)]
    public class AGeometryEditor : Editor
    {
        private static readonly GUIContent AddTriggerButtonContent = new GUIContent("Add Trigger", "Adds a trigger (disabled if Trigger already exists)");

        protected AGeometry geometry;

        private void OnEnable()
        {
            geometry = target as AGeometry;
        }

        public override void OnInspectorGUI()
        {
            List<string> helpBoxContent = new List<string>
            {
                "GeoJSON ID: " + (geometry.JsonId != "" ? geometry.JsonId : "**none**")
            };
            EditorGUILayout.HelpBox(string.Join("\n", helpBoxContent.ToArray()), MessageType.Info);
            EditorGUILayout.Separator();
            
            EditorGUILayout.Separator();

            CenterTransformButton();
            EditorGUILayout.Separator();
            AddTriggerButton();

            serializedObject.ApplyModifiedProperties();
        }

        private void CenterTransformButton()
        {
            if (GUILayout.Button("Center transform"))
            {
                var worldCoordinates = geometry.WorldCoordinates;
                geometry.transform.position = geometry.WorldBounds.center;
                geometry.WorldCoordinates = worldCoordinates;
            }
        }

        private void AddTriggerButton()
        {
            // this button adds a trigger component to the gameobject
            if (geometry.gameObject.GetComponent<ATrigger>() != null)
                GUI.enabled = false;
            if (GUILayout.Button(AddTriggerButtonContent))
            {
                AddTrigger();
            }
            GUI.enabled = true;
        }

        protected virtual void AddTrigger()
        {
            throw new NotImplementedException();
        }
     }
}