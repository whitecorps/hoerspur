﻿namespace Hoerspur.Geometry
{
    using Hoerspur.Audio;
    using Hoerspur.Triggers;
    using Hoerspur.Utils;
    using System;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(PointGeometry))]
    public class PointGeometryEditor : AGeometryEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            AddAudioTargetButton();
        }

        private void AddAudioTargetButton()
        {
            if (geometry.gameObject.GetComponent<AudioPlayer>() != null)
                GUI.enabled = false;
            if (GUILayout.Button("Add AudioPlayer"))
            {
                AddAudioPlayer();
            }
            GUI.enabled = true;
        }

        protected override void AddTrigger()
        {
            Undo.AddComponent<PointTrigger>(geometry.gameObject);
            //geometry.gameObject.AddComponent<PointTrigger>();
        }

        private void AddAudioPlayer()
        {
            Undo.AddComponent<AudioPlayer>(geometry.gameObject);
            //geometry.gameObject.AddComponent<AudioTarget>();
        }

        //protected override void UpdateMesh()
        //{
        //    var trigger = geometry.GetComponent<PointTrigger>();
        //    if (trigger)
        //    {
        //        // create mesh using radius
        //        var vertices = new List<Vector2>();
        //        for (int i = 0; i < CircleResolution; i++)
        //        {
        //            var x = Mathf.PI * 2f * i / CircleResolution;
        //            vertices.Add(new Vector2(Mathf.Cos(x), Mathf.Sin(x)) * trigger.radius);
        //        }
        //        UpdateCircleMesh(vertices);
        //    }
        //    else
        //    {
        //        RemoveMesh();
        //    }
        //}

        //private void UpdateCircleMesh(List<Vector2> verts)
        //{
        //    var mf = geometry.GetComponent<MeshFilter>();
        //    if (!mf)
        //        mf = geometry.gameObject.AddComponent<MeshFilter>();

        //    if (mf.sharedMesh == null)
        //        mf.sharedMesh = new Mesh();
        //    else
        //        mf.sharedMesh.Clear();

        //    mf.sharedMesh.vertices = Array.ConvertAll(verts.ToArray(), p => new Vector3(p.x, p.y, MeshOffset)); // Vector2 -> Vector3
        //    mf.sharedMesh.triangles = Triangulator.Triangulate(verts);
        //    mf.sharedMesh.name = geometry.name + "_Circle";

        //    var mr = geometry.GetComponent<MeshRenderer>();
        //    if (!mr)
        //        mr = geometry.gameObject.AddComponent<MeshRenderer>();

        //    mr.material = geometry.Material;
        //}
    }
}