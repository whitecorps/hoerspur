﻿namespace Hoerspur.Geometry
{
    using Hoerspur.Utils;
    using System.Collections.Generic;
    using UnityEngine;

    public class PolygonGeometry : AGeometry
    {
        //public override Vector3 GetWorldPosition()
        //{
        //    var offsetBounds = GeoLocationHelper.GetBounds(LocalCoordinates);
        //    offsetBounds.center += transform.localPosition.XY();
        //    var position = GeoLocationHelper.GetBounds(LocalCoordinates).center;
        //    return new Vector3(offsetBounds.center.x, offsetBounds.center.y, 0);
        //}

        private void Reset()
        {
            // set a reasonable default to avoid weird errors
            LocalCoordinates = new List<Vector2>()
            {
                new Vector2(-5, -5),
                new Vector2( 5, -5),
                new Vector2( 5,  5),
                new Vector2(-5,  5)
            };
        }
    }
}