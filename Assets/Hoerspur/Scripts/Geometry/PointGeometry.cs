﻿namespace Hoerspur.Geometry
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class PointGeometry : AGeometry
    {
        private void Reset()
        {
            // set a reasonable default to avoid weird errors
            LocalCoordinates = new List<Vector2>()
            {
                new Vector2(0,0)
            };
        }
    }
}
