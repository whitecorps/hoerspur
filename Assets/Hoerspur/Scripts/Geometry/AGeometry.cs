﻿namespace Hoerspur.Geometry
{
    using Hoerspur.Utils;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class AGeometry : MonoBehaviour
    {
        [SerializeField]
        private string jsonId = "";
        [SerializeField]
        private List<Vector2> localCoordinates;
        //[SerializeField]
        //private bool showMeshInGame = true;
        [SerializeField]
        private Material material;

        public string JsonId
        {
            get { return jsonId; }
            set
            {
                if (jsonId != "" && jsonId != value)
                    Debug.LogWarning("JsonId is being changed from '" + jsonId + "' to '" + jsonId + "'; this should not happen!");
                jsonId = value;
            }
        }

        public List<Vector2> LocalCoordinates
        {
            get { return localCoordinates; }
            set { localCoordinates = value; }
        }

        public Material Material
        {
            get { return material; }
            set { material = value; }
        }

        public List<Vector2> WorldCoordinates
        {
            get
            {
                if (localCoordinates == null)
                    return null;

                var result = new List<Vector2>();
                foreach (var coord in localCoordinates)
                {
                    result.Add(coord + transform.position.AsVector2());
                }
                return result;
            }
            set
            {
                if (localCoordinates == null)
                    localCoordinates = new List<Vector2>();

                localCoordinates.Clear();
                foreach (var coord in value)
                {
                    localCoordinates.Add(coord - transform.position.AsVector2());
                }
            }
        }

        public Bounds LocalBounds
        {
            get { return GeoLocationHelper.GetBounds(localCoordinates); }
        }

        public Bounds WorldBounds
        {
            get
            {
                var worldBounds = LocalBounds;
                worldBounds.center  += transform.localPosition;
                return worldBounds;
            }
        }
    }
}