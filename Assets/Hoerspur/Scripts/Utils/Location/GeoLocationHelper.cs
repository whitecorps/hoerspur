﻿namespace Hoerspur.Utils
{
    using System.Collections.Generic;
    using UnityEngine;

    public static class GeoLocationHelper
    {
        /// <summary>
        /// Calculates the distance between two lon/lat coordinates
        /// Source: https://www.movable-type.co.uk/scripts/latlong.html
        /// </summary>
        /// <param name="p1">The first coordinate (lon/lat)</param>
        /// <param name="p2">The second coordinate (lon/lat)</param>
        /// <returns>The distance between the two coordinates (in meters)</returns>
        public static float Distance(Vector2 p1, Vector2 p2)
        {
            return Distance(p1.x, p1.y, p2.x, p2.y);
        }

        /// <summary>
        /// Calculates the distance between to lon/lat coordinates
        /// Source: https://www.movable-type.co.uk/scripts/latlong.html
        /// </summary>
        /// <param name="lon1">Longitude of first coordinate</param>
        /// <param name="lat1">Latitude of first coordinate</param>
        /// <param name="lon2">Longitude of second coordinate</param>
        /// <param name="lat2">Latitude of second coordinate</param>
        /// <returns>The distance between the two coordinates (in meters)</returns>
        public static float Distance(float lon1, float lat1, float lon2, float lat2)
        {
            var lr1 = lat1 * Mathf.Deg2Rad;
            var lr2 = lat2 * Mathf.Deg2Rad;
            var dlat = (lat2 - lat1) * Mathf.Deg2Rad;
            var dlon = (lon2 - lon1) * Mathf.Deg2Rad;

            var a = Mathf.Sin(dlat / 2) * Mathf.Sin(dlat / 2) +
                Mathf.Cos(lr1) * Mathf.Cos(lr2) *
                Mathf.Sin(dlon / 2) * Mathf.Sin(dlon / 2);
            var c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));

            return c * 6371e3f;
        }

        public static Bounds GeoToWorldBounds(Bounds geoBounds, float scale = 1f)
        {
            Vector2 topLeft = new Vector2(geoBounds.min.x, geoBounds.max.y);
            Vector2 topRight = geoBounds.max;
            Vector2 bottomLeft = geoBounds.min;
            Vector2 bottomRight = new Vector2(geoBounds.max.x, geoBounds.min.y);
            float height = Distance(bottomLeft, topLeft);
            float topWidth = Distance(topLeft, topRight);
            float bottomWidth = Distance(bottomLeft, bottomRight);
            float width = Mathf.Min(topWidth, bottomWidth);
            return new Bounds(Vector2.zero, new Vector2(width, height));
        }

        public static Vector2 ConvertCoordinates(Bounds from, Bounds to, Vector2 coordinates)
        {
            float relativeX = Mathf.InverseLerp(from.min.x, from.max.x, coordinates.x);
            float relativeY = Mathf.InverseLerp(from.min.y, from.max.y, coordinates.y);
            float targetX = Mathf.Lerp(to.min.x, to.max.x, relativeX);
            float targetY = Mathf.Lerp(to.min.y, to.max.y, relativeY);
            return new Vector2(targetX, targetY);
        }

        public static List<Vector2> ConvertCoordinates(Bounds from, Bounds to, List<Vector2> coordinates)
        {
            List<Vector2> result = new List<Vector2>();
            foreach (var coord in coordinates)
            {
                result.Add(ConvertCoordinates(from, to, coord));
            }
            return result;
        }

        public static bool PolygonContainsPoint(List<Vector2> poly, Vector2 point)
        {
            int j = poly.Count - 1;
            bool inside = false;
            for (int i = 0; i < poly.Count; j = i++)
            {
                if (((poly[i].y <= point.y && point.y < poly[j].y) || (poly[j].y <= point.y && point.y < poly[i].y)) &&
                    (point.x < (poly[j].x - poly[i].x) * (point.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x))
                    inside = !inside;
            }
            return inside;
        }

        public static Vector3 V2ToV3(Vector2 v2, float height = 0)
        {
            return new Vector3(v2.x, height, v2.y);
        }

        public static Vector2 V3ToV2(Vector3 v3)
        {
            return new Vector2(v3.x, v3.z);
        }

        public static Bounds GetBounds(List<Vector2> points)
        {
            if (points.Count == 0)
                throw new UnityException("List is empty, cannot calculate Bounds");

            var result = new Bounds(points[0], Vector2.zero);
            for (int i = 1; i < points.Count; i++)
            {
                result.Encapsulate(points[i]);
            }

            return result;
        }
    }

    public class CoordinatesConverter
    {
        public Bounds from;
        private Bounds to;

        public CoordinatesConverter(Bounds from, Bounds to)
        {
            this.from = from;
            this.to = to;
        }

        public Vector2 Convert(Vector2 vec)
        {
            return GeoLocationHelper.ConvertCoordinates(from, to, vec);
        }

        public List<Vector2> Convert(List<Vector2> vec)
        {
            return GeoLocationHelper.ConvertCoordinates(from, to, vec);
        }

        public Vector2 ConvertInverse(Vector2 vec)
        {
            return GeoLocationHelper.ConvertCoordinates(to, from, vec);
        }

        public List<Vector2> ConvertInverse(List<Vector2> vec)
        {
            return GeoLocationHelper.ConvertCoordinates(to, from, vec);
        }
    }
}