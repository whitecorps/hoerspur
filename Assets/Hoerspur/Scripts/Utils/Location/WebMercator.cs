﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WebMercator
{
    public const int TileSize = 512;
    private const int EarthRadius = 6378137;
    private const float InitialResolution = 2 * Mathf.PI * EarthRadius / TileSize;
    private const float OriginShift = 2 * Mathf.PI * EarthRadius / 2;

    // converts lat/lon in WSG84 to XY in Spherical Mercator EPSG:900913
    public static Vector2 LatLonToMeters(float lat, float lon)
    {
        return new Vector2()
        {
            x = lon * OriginShift / 180,
            y = (Mathf.Log(Mathf.Tan((90 + lat) * Mathf.PI / 360)) / (Mathf.PI / 180))
                * OriginShift / 180
        };
    }

    // Converts XY from (Spherical) Web Mercator EPSG:3785 
    public static Vector2 MetersToLatLon(Vector2 meters)
    {
        return new Vector2()
        {
            x = (meters.x / OriginShift) * 180,
            y = 180 / Mathf.PI * (2 * Mathf.Atan(Mathf.Exp((meters.y / OriginShift) * Mathf.PI / 180)) - Mathf.PI / 2)
        };
    }

    // converts pixel coordinates in given zoom level of pyramid to EPSG:900913
    public static Vector2 PixelsToMeters(Vector2 pixels, int zoom)
    {
        var res = Resolution(zoom);
        return new Vector2()
        {
            x = pixels.x * res - OriginShift,
            y = pixels.y * res - OriginShift
        };
    }

    // converts EPSG:900913 to pyramid level pixel coordinates in given zoom level
    public static Vector2 MetersToPixels(Vector2 meters, int zoom)
    {
        var res = Resolution(zoom);
        return new Vector2()
        {
            x = (meters.x + OriginShift) / res,
            y = (meters.y + OriginShift) / res
        };
    }

    // returns a TMS (not Google!) tile covering region in given pixel coordinates
    public static Tile PixelsToTile(Vector2 pixels)
    {
        return new Tile()
        {
            X = Mathf.CeilToInt(pixels.x / TileSize) - 1,
            Y = Mathf.CeilToInt(pixels.y / TileSize) - 1
        };
    }

    public static Vector2 PixelsToRaster(Vector2 pixels, int zoom)
    {
        var mapSize = TileSize << zoom;
        return new Vector2(pixels.x, mapSize - pixels.y);
    }

    // returns tile for given mercator coordinates
    public static Tile MetersToTile(Vector2 meters, int zoom)
    {
        var pixels = MetersToPixels(meters, zoom);
        return PixelsToTile(pixels);
    }

    // returns bounds of the given tile in EPSG:900913 coordinates
    public static Rect TileBounds(Tile t, int zoom)
    {
        var pos = PixelsToMeters(new Vector2(t.X * TileSize, t.Y * TileSize), zoom);
        var size = PixelsToMeters(new Vector2(TileSize, TileSize), zoom);
        //var max = PixelsToMeters(new Vector2((t.X + 1) * TileSize, (t.Y + 1) * TileSize), zoom);
        return new Rect(pos, size);
    }

    // returns bounds of the tile in lat/lon using WSG84
    public static Rect TileLatLonBounds(Tile t, int zoom)
    {
        var bounds = TileBounds(t, zoom);
        var pos = MetersToLatLon(bounds.min);
        var size = MetersToLatLon(bounds.size);
        return new Rect(pos, size);
    }

    // returns resolution (meters/pixel) for given zoom level (at equator)
    public static float Resolution(int zoom)
    {
        return InitialResolution / Mathf.Pow(2, zoom);
    }


    public static int ZoomForPixelSize(float pixelSize)
    {
        for (var i = 0; i < 30; i++)
        {
            if (pixelSize > Resolution(i))
                return i != 0 ? i - 1 : 0;
        }
        throw new InvalidOperationException();
    }

    // convert TMS tile to Google Tile
    public static Tile ToGoogleTile(Tile t, int zoom)
    {
        return new Tile(t.X, ((int)Mathf.Pow(2, zoom) - 1) - t.Y);
    }

    public static float GetDistance(Vector2 latlng1, Vector2 latlng2)
    {
        float
            lat1 = latlng1.x * Mathf.Deg2Rad,
            lat2 = latlng2.x * Mathf.Deg2Rad,
            a = Mathf.Sin(lat1) * Mathf.Sin(lat2) +
                Mathf.Cos(lat1) * Mathf.Cos(lat2) * Mathf.Cos((latlng1.y - latlng2.y) * Mathf.Deg2Rad),
            meters = EarthRadius * Mathf.Acos(Mathf.Min(a, 1));
        return meters;
    }
}

/// <summary>
/// Reference to a Tile X, Y index
/// </summary>
public class Tile
{
    public Tile() { }
    public Tile(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int X { get; set; }
    public int Y { get; set; }
}
