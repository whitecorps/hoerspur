﻿namespace Hoerspur.Utils
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class Singleton<T> : MonoBehaviour where T : Component
    {
        #region Fields

        /// <summary>
        /// The instance.
        /// </summary>
        private static T instance;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<T>();
                    if (instance == null && CreateNewIfNotFound)
                    {
                        Debug.LogError("No instance of " + typeof(T).ToString() + " found, creating new!");
                        GameObject obj = new GameObject();
                        obj.name = typeof(T).Name;
                        instance = obj.AddComponent<T>();
                    }
                }
                return instance;
            }
        }

        public static bool CreateNewIfNotFound;
        #endregion

        #region Methods

        /// <summary>
        /// Use this for initialization.
        /// </summary>
        protected virtual void Awake()
        {
            if (instance == null)
            {
                instance = this as T;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Debug.LogWarning("Multiple instances of " + typeof(T).ToString() + " found, deleting", this);
                Destroy(gameObject);
            }
        }

        #endregion
    }
}