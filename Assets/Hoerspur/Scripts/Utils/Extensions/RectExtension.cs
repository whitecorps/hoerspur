﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RectExtension
{
    public const int LineHeight = 16;
    public const int DefaultSpacing = 2;    // for both directions

    /// <summary>
    /// Horizontally scale and offset a rect using percent values
    /// </summary>
    /// <param name="rect"></param>
    /// <param name="x">Offset from the left in percent [0,1]</param>
    /// <param name="width">Width as percentage of old width [0,1]</param>
    /// <param name="horizontalSpacing">Width is reduced by this value to create a space between this rect and following rects [in pixels]</param>
    /// <returns></returns>
    public static Rect Select(this Rect rect, float x, float width, int horizontalSpacing = DefaultSpacing)
    {
        var initialWidth = rect.width;
        rect.xMin += rect.width * x;
        rect.width = initialWidth * width;
        rect.width -= horizontalSpacing;
        return rect;
    }

    /// <summary>
    /// Moves the rect down by one line
    /// </summary>
    /// <param name="rect"></param>
    /// <param name="horizontalSpacing">Spacing between lines</param>
    /// <returns></returns>
    public static Rect NextLine(this Rect rect, int horizontalSpacing = DefaultSpacing)
    {
        rect.yMin += LineHeight + horizontalSpacing;
        rect.yMax += LineHeight + horizontalSpacing;
        return rect;
    }

    /// <summary>
    /// Sets rect height to exactly one line
    /// </summary>
    /// <param name="rect"></param>
    /// <returns></returns>
    public static Rect FirstLine(this Rect rect)
    {
        rect.height = LineHeight;
        return rect;
    }
}
