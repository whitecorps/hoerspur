﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorExtension
{
    // flips a vector2
    public static Vector2 YX(this Vector2 v)
    {
        return new Vector2(v.y, v.x);
    }

    // flips x and y of a vector3
    public static Vector3 YX(this Vector3 v)
    {
        return new Vector3(v.y, v.x, v.z);
    }

    // returns a vector2 with xy components only
    public static Vector3 XY(this Vector3 v)
    {
        return new Vector3(v.x, v.y);
    }

    public static Vector3 AsVector3(this Vector2 v)
    {
        return new Vector3(v.x, v.y);
    }

    public static Vector2 AsVector2(this Vector3 v)
    {
        return new Vector2(v.x, v.y);
    }
}
