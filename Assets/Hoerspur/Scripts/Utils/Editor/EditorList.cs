﻿namespace Hoerspur.Utils
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    using System;
    using System.Linq;

    [Flags]
    public enum EditorListOptions
    {
        None = 0,
        ListSize = 1,
        ListLabel = 2,
        ElementLabels = 4,
        Buttons = 8,
        BoxedElements = 16,
        Default = ListSize | ListLabel | ElementLabels,
        NoElementLabels = ListSize | ListLabel,
        All = Default | Buttons,
    }

    public enum EditorListButtonOptions
    {
        None = 0,
        Move = 1,
        Duplicate = 2,
        Delete = 4,
        NoDuplicate = Move | Delete,
        All = Move | Duplicate | Delete,
    }

    public static class EditorList
    {
        private static readonly GUILayoutOption miniButtonWidth = GUILayout.Width(20f);
        private static readonly GUIContent
            moveUpButtonContent = new GUIContent("\u2196", "move up"),
            moveDownButtonContent = new GUIContent("\u2198", "move down"),
            duplicateButtonContent = new GUIContent("+", "add element here"),
            deleteButtonContent = new GUIContent("X", "delete"),
            addButtonContent = new GUIContent("+", "add element");

        private static List<string> errors = new List<string>();

        private static bool styleInit = false;
        private static GUIStyle[] styles = {
                (new GUIStyle(EditorStyles.helpBox)),
                (new GUIStyle(EditorStyles.helpBox))
        };

        public static void Show(
            SerializedProperty list,
            EditorListOptions options = EditorListOptions.Default,
            EditorListButtonOptions btnOptions = EditorListButtonOptions.All,
            Func<SerializedProperty, int, string> errorCheck = null,
            Action<SerializedProperty> onCreate = null,
            Action<SerializedProperty, SerializedProperty, int> singlePropertyDrawer = null,
            Action<SerializedProperty> addButtonDrawer = null
            )
        {
            if (!list.isArray)
            {
                EditorGUILayout.HelpBox(list.name + " is neither an array nor a list!", MessageType.Error);
                return;
            }

            if (!styleInit)
            {
                styles[0].normal.background = MakeTex(2, 2, new Color(.95f, .95f, .95f));
                styles[1].normal.background = MakeTex(2, 2, new Color(.75f, .75f, .75f));
                styleInit = true;
            }

            bool
                showListLabel = (options & EditorListOptions.ListLabel) != 0,
                showListSize = (options & EditorListOptions.ListSize) != 0;

            if (showListLabel)
            {
                EditorGUILayout.PropertyField(list);
                EditorGUI.indentLevel += 1;
            }
            if (!showListLabel || list.isExpanded)
            {
                SerializedProperty size = list.FindPropertyRelative("Array.size");
                if (showListSize)
                {
                    EditorGUILayout.PropertyField(size);
                }
                if (size.hasMultipleDifferentValues)
                {
                    EditorGUILayout.HelpBox("Not showing list with different sizes.", MessageType.Info);
                }
                else
                {
                    ShowElements(list, options, btnOptions, errorCheck, onCreate, singlePropertyDrawer, addButtonDrawer);
                }
            }

            // show errors
            if (errors.Count > 0)
            {
                errors.Insert(0, "The following errors occured:\n");
                EditorGUILayout.HelpBox(string.Join("\n- ", errors.Distinct().ToArray()), MessageType.Error);
            }

            if (showListLabel)
            {
                EditorGUI.indentLevel -= 1;
            }
        }

        private static void ShowElements(
            SerializedProperty list,
            EditorListOptions options,
            EditorListButtonOptions btnOptions,
            Func<SerializedProperty, int, string> errorCheck = null,
            Action<SerializedProperty> onCreate = null,
            Action<SerializedProperty, SerializedProperty, int> singlePropertyDrawer = null,
            Action<SerializedProperty> addButtonDrawer = null
            )
        {
            Color defaultGUIColor = GUI.backgroundColor;
            bool
                showElementLabel = (options & EditorListOptions.ElementLabels) != 0,
                showButtons = (options & EditorListOptions.Buttons) != 0,
                boxedElements = (options & EditorListOptions.BoxedElements) != 0;

            errors.Clear();

            for (int i = 0; i < list.arraySize; i++)
            {
                if (boxedElements)
                {
                    EditorGUILayout.BeginVertical(styles[i % 2]);
                }
                if (showButtons)
                {
                    EditorGUILayout.BeginHorizontal();
                }

                if (errorCheck != null)
                {
                    string error = errorCheck(list, i);
                    if (error != "")
                    {
                        errors.Add(error);
                        GUI.backgroundColor = new Color(1, .7f, .7f, 1f);
                    }
                }

                if (singlePropertyDrawer == null)
                {
                    if (showElementLabel)
                    {
                        EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), true);
                    }
                    else
                    {
                        EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), GUIContent.none, true);
                    }
                }
                else
                {
                    singlePropertyDrawer(list, list.GetArrayElementAtIndex(i), i);
                }

                GUI.backgroundColor = defaultGUIColor;
                if (showButtons)
                {
                    ShowButtons(list, i, btnOptions, onCreate);
                    EditorGUILayout.EndHorizontal();
                }
                if (boxedElements)
                {
                    EditorGUILayout.EndVertical();
                }
            }
            if (list.arraySize > 0)
            {
                EditorGUILayout.Space();
            }
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(EditorGUI.indentLevel * 15);
            //if (GUILayout.Button(addButtonContent))
            //{
            //    list.arraySize += 1;
            //    if (onCreate != null)
            //        onCreate(list.GetArrayElementAtIndex(list.arraySize - 1));
            //}


            // do we have a custom add button drawer?
            // if not use the built in standard add ²button drawer
            SerializedProperty newListItem = null;
            if (addButtonDrawer != null)
            {
                addButtonDrawer(list);
            }
            else
            {
                newListItem = DefaultAddButtonDrawer(list);
                if (onCreate != null && newListItem != null)
                    onCreate(newListItem);
            }


            EditorGUILayout.EndHorizontal();
        }

        private static SerializedProperty DefaultAddButtonDrawer(SerializedProperty list)
        {
            if (GUILayout.Button(addButtonContent))
            {
                list.arraySize += 1;
                return list.GetArrayElementAtIndex(list.arraySize - 1);
            }
            return null;
        }

        public static bool ShowButtons(SerializedProperty list, int index, EditorListButtonOptions options, Action<SerializedProperty> onCreate = null)
        {
            var showMoveButtons = (options & EditorListButtonOptions.Move) != 0;
            var showDuplicateButton = (options & EditorListButtonOptions.Duplicate) != 0;
            var showDeleteButton = (options & EditorListButtonOptions.Delete) != 0;
            if (showMoveButtons && GUILayout.Button(moveUpButtonContent, EditorStyles.miniButtonLeft, miniButtonWidth))
            {
                list.MoveArrayElement(index, index - 1);
            }
            if (showMoveButtons && GUILayout.Button(moveDownButtonContent, EditorStyles.miniButtonRight, miniButtonWidth))
            {
                list.MoveArrayElement(index, index + 1);
            }
            if (showDuplicateButton && GUILayout.Button(duplicateButtonContent, EditorStyles.miniButton, miniButtonWidth))
            {
                list.InsertArrayElementAtIndex(index);
                if (onCreate != null)
                    onCreate(list.GetArrayElementAtIndex(list.arraySize - 1));
            }
            if (showDeleteButton && GUILayout.Button(deleteButtonContent, EditorStyles.miniButton, miniButtonWidth))
            {
                int oldArraySize = list.arraySize;
                list.DeleteArrayElementAtIndex(index);
                if (list.arraySize == oldArraySize)
                {
                    list.DeleteArrayElementAtIndex(index);
                }
                return true;
            }
            return false;
        }

        private static Texture2D MakeTex(int width, int height, Color col)
        {
            var pix = new Color[width * height];

            for (int i = 0; i < pix.Length; i++)
            {
                pix[i] = col;
            }

            var result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();
            return result;
        }
    }
}
