﻿namespace Hoerspur.Utils
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public static class EditorHelper
    {
        public const int IndentWidth = 15;
        public const int LineHeight = 18;
        public const int LineHeightNoPadding = 16;

        public static readonly Color GizmoColorActive = new Color(0.2f, 0.2f, 1f, 0.4f);
        public static readonly Color GizmoColor = new Color(0.2f, 0.2f, 1f, 0.2f);

        public static readonly Color GizmoLineColorActive = Color.white;
        public static readonly Color GizmoLineColor = Color.black;
        //public static readonly Color GizmoLineColor = new Color(1f, 1f, 1f, 0.6f);

        public static Rect ResizeRectHorizontal(Rect rect, float x, float width)
        {
            rect.x += rect.width * x;
            rect.width *= width;
            return rect;
        }

        public static Rect ResizeRectVertical(Rect rect, float y, float height)
        {
            rect.y += rect.height * y;
            rect.width *= height;
            return rect;
        }

        public static Rect ResizeRect(Rect rect, float x, float y, float width, float height)
        {
            return ResizeRectVertical(ResizeRectHorizontal(rect, x, width), y, height);
        }

        public static Rect ResizeRectHorizontalAbsolute(Rect rect, int x, int width)
        {
            rect.x += x;
            rect.width = width;
            return rect;
        }

        public static Rect ResizeRectVerticalAbsolute(Rect rect, int y, int height)
        {
            rect.y += y;
            rect.height = height;
            return rect;
        }

        public static Rect ResizeRectAbsolute(Rect rect, int x, int y, int width, int height)
        {
            return ResizeRectVertical(ResizeRectHorizontal(rect, x, width), y, height);
        }

        public static Rect ResizeRectLines(Rect rect, int lines, int skipFromTop)
        {
            int height = (lines - 1) * LineHeight + LineHeightNoPadding;
            if (lines < 1)
                height = 0;
            return ResizeRectVerticalAbsolute(rect, skipFromTop * LineHeight, height);
        }

        public static void HeaderLabel(string label)
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
            EditorGUILayout.EndHorizontal();
        }

        public static Vector3 MouseToWorldPosition()
        {
            var mousePos = new Vector2(Event.current.mousePosition.x, Camera.current.pixelHeight - Event.current.mousePosition.y);
            return Camera.current.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, Camera.current.nearClipPlane));
        }
    }
}