﻿using Hoerspur.Audio;
using Hoerspur.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class RolloffVisualizer
{

    private static AnimationCurve lin;
    private static AnimationCurve log;
    private static float maxDist = 0;
    public static Rect DrawRolloff(float height, float maxDistance, float blend)
    {
        var rect = EditorGUILayout.GetControlRect(false, height);
        var indentWidth = EditorGUI.indentLevel * EditorHelper.IndentWidth;
        rect.xMin += indentWidth;
        if (lin == null || maxDistance != maxDist)
        {
            lin = Rolloff2D.CreateLinearRolloff(maxDistance);
            maxDist = maxDistance;
        }
        if (log == null)
            log = Rolloff2D.CreateLogarithmicRolloff();

        EditorGUI.DrawRect(rect, Color.gray);
        for (int x = 0; x < rect.width; x++)
        {
            var distance = Mathf.Lerp(0, maxDistance, Mathf.InverseLerp(0, rect.width, x));
            var volume = Rolloff2D.CalculateVolume(lin, log, distance, blend);
            var y = 80f - Mathf.Lerp(0, 80f, volume);
            EditorGUI.DrawRect(new Rect(rect.x + x, rect.y + y, 1, 1), Color.black);
        }
        return rect;
    }

    public static void DrawRolloff(float height, float maxDistance, float blend, float currentDistance)
    {
        var rect = DrawRolloff(height, maxDistance, blend);
        var x = Mathf.Lerp(rect.xMin, rect.xMax, Mathf.InverseLerp(0, maxDistance, currentDistance));
        EditorGUI.DrawRect(new Rect(rect.x + x, rect.y, 1, height), Color.red);
    }
}
