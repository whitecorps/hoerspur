﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SingletonSimple<T> : MonoBehaviour where T : Component {
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType<T>();

                if (!_instance)
                {
                    Debug.Log("No instance of " + typeof(T).ToString() + " found");
                }
            }
            return _instance;
        }
    }
}
