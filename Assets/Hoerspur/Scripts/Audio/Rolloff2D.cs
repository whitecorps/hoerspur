﻿namespace Hoerspur.Audio
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [RequireComponent(typeof(AudioSource))]
    public class Rolloff2D : MonoBehaviour
    {
        // manual falloff curve for logarithmic rolloff (realistic)
        private static readonly AnimationCurve RolloffLog = new AnimationCurve(
            new Keyframe(1, 1f),
            new Keyframe(2, 0.5f),
            new Keyframe(4, 0.25f),
            new Keyframe(8, 0.125f),
            new Keyframe(16, 0.0625f),
            new Keyframe(32, 0.03125f),
            new Keyframe(62, 0.015625f),
            new Keyframe(128, 0.0078125f),
            new Keyframe(256, 0.00390625f),
            new Keyframe(512, 0.001953125f),
            new Keyframe(1024, 0f)
        );

        public AudioListener audioListener;
        public AudioSource audioSource;
        [Range(0f, 1f), Tooltip("At 0 the rolloff is strictly linear, at 1 the rolloff is strictly logarithmic")]
        public float rolloffBlend = 0;
        public float maxDistance = 500f;

        private AnimationCurve rolloffLog;
        private AnimationCurve rolloffLinear;

        private void Awake()
        {
            audioListener = FindObjectOfType<AudioListener>();
            audioSource = GetComponent<AudioSource>();

        }

        private void Start()
        {
            rolloffLog = CreateLogarithmicRolloff();
            rolloffLinear = CreateLinearRolloff(maxDistance);
        }

        private void Update()
        {
            var distance = Vector3.Distance(audioListener.transform.position, transform.position);
            var volume = CalculateVolume(distance);
            audioSource.volume = volume;
        }

        private float CalculateVolume(float distance)
        {
            return CalculateVolume(rolloffLinear, rolloffLog, distance, rolloffBlend);
        }

        /// <summary>
        /// Creates a linear rolloff curve
        /// </summary>
        /// <param name="maxDistance">At this distance the volume reaches 0</param>
        /// <returns></returns>
        public static AnimationCurve CreateLinearRolloff(float maxDistance)
        {
            var result = new AnimationCurve(
                new Keyframe(1f, 1f),
                new Keyframe(maxDistance, 0f)
            );
            SmoothTangents(result);
            return result;
        }

        /// <summary>
        /// Creates a logarithmic rolloff curve
        /// </summary>
        /// <returns></returns>
        public static AnimationCurve CreateLogarithmicRolloff()
        {
            var result = RolloffLog;
            SmoothTangents(result);
            return result;
        }

        public static float CalculateVolume(AnimationCurve curve1, AnimationCurve curve2, float distance, float lerp)
        {
            return Mathf.Lerp(curve1.Evaluate(distance), curve2.Evaluate(distance), lerp);
        }

        private static void SmoothTangents(AnimationCurve curve)
        {
            for (int i = 0; i < curve.keys.Length; ++i)
            {
                curve.SmoothTangents(i, 0); //zero weight means average
            }
        }
    }
}
