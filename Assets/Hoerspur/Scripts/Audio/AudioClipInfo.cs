﻿namespace Hoerspur.Audio
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Audio;

    [System.Serializable]
    public class AudioClipInfo : IEquatable<AudioClipInfo>
    {
        public enum RolloffCalculation
        {
            None,
            AttenuationOnly,
            AttenuationAndDirection
        }

        public AudioClip clip;
        [HideInInspector]
        public AudioPlayer player;
        public AudioMixerGroup mixer;
        public AudioClipPriority priority;
        public bool loop;       // only makes sense if prio is background
        [HideInInspector]
        public Action<AudioClipInfo> callback;
        [HideInInspector]
        public AudioSource audioSourceComponent;
        [HideInInspector]
        public bool addToLog;
        [HideInInspector]
        public string text;

        [Tooltip("None: Sound is 2D, so no rolloff with distance and no way to locate sound in space\n\nAttenuation Only: Sound is 2D, but fades with distance\n\nAttenuation And Direction: Sound is 3D and handled by Unitys spatial audio calculation")]
        public RolloffCalculation rolloffCalculation;
        [Range(0f, 1f), Tooltip("The actual rolloff can be a blend between linear and logarithmic. 0 is linear, 1 is logarithmic.")]
        public float rolloffBlend;
        [Tooltip("At this distance the audio volume will reach zero.")]
        public float rolloffMaxDistance = 500;

        private bool paused = false;

        public bool Paused
        {
            get
            {
                return paused;
            }
            set
            {
                PausePlayback(value);
            }
        }

        public AudioClipInfo() { }

        public AudioClipInfo Clone()
        {
            var clone = new AudioClipInfo()
            {
                clip = clip,
                player = player,
                mixer = mixer,
                priority = priority,
                loop = loop,
                callback = null,
                audioSourceComponent = null,
                addToLog = addToLog,
                rolloffCalculation = rolloffCalculation,
                rolloffMaxDistance = rolloffMaxDistance,
                rolloffBlend = rolloffBlend,
                text = text,
            };
            return clone;
        }

        public void StartPlayback()
        {
            player.PlayAudio(this);
        }

        public void StopPlayback()
        {
            // this will stop playback, callback will be called from AudioPlayer
            if (audioSourceComponent != null)
                audioSourceComponent.Stop();
        }

        public void Restart()
        {
            if (audioSourceComponent != null)
            {
                audioSourceComponent.time = 0;
            }
        }

        public void PausePlayback(bool paused)
        {
            if (audioSourceComponent != null)
            {
                this.paused = paused;
                if (paused)
                    audioSourceComponent.Pause();
                else
                    audioSourceComponent.UnPause();
            }
            else
            {
                Debug.LogWarning("Trying to pause audio without AudioSource component, doing nothing");
                paused = false;
            }
        }

        /// <summary>
        /// Compares both text and clip properties for equality. Other properties only contain technical parameters, not content.
        /// </summary>
        public bool Equals(AudioClipInfo other)
        {
            var textEq = text == other.text;
            var clipEq = clip == other.clip;
            return (other.text == text && other.clip == clip);
        }


    }
}
