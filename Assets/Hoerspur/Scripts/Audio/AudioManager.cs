﻿namespace Hoerspur.Audio
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.Audio;
    using UnityEngine.Events;

    public class AudioManager : SingletonSimple<AudioManager>
    {
        private bool playing;
        private Queue<AudioClipInfo> queue;
        private AudioClipInfo queueCurrent;
        private AudioClipInfo lastQueuedItem;

        public AudioMixerGroup defaultMixerQueue;
        public AudioMixerGroup defaultMixerQueueImmediate;
        public AudioMixerGroup defaultMixerBackground;

        public NewAudioLogEvent onNewAudioLog;

        private List<AudioLogEntry> log;    // should this live in audiomanager or get its own class?


        public bool HasCurrentClip
        {
            get { return queueCurrent != null; }
        }

        public bool Paused
        {
            get { return (queueCurrent != null && queueCurrent.Paused); }
        }

        public List<AudioLogEntry> AudioLog
        {
            get { return log; }
        }

        private void Awake()
        {
            queue = new Queue<AudioClipInfo>();
            log = new List<AudioLogEntry>();
        }

        public void Add(AudioClipInfo info)
        {
            AssignAudioMixerGroup(info);
            switch (info.priority)
            {
                case AudioClipPriority.Queue:
                    Enqueue(info);
                    break;
                case AudioClipPriority.QueueImmediate:
                    while (queue.Count > 0)
                    {
                        var skipped = queue.Dequeue();
                        AddToLog(skipped);
                    }

                    if (queueCurrent != null)
                    {
                        queueCurrent.callback = null;   // remove callback, so we dont set 
                        queueCurrent.StopPlayback();
                    }
                    queueCurrent = null;

                    Enqueue(info);
                    break;
                case AudioClipPriority.Background:
                    info.player.PlayAudio(info);
                    break;
                default:
                    Debug.LogError("Unknown AudioClipPriority", this);
                    break;
            }
        }

        public void Pause()
        {
            if (queueCurrent != null)
            {
                queueCurrent.PausePlayback(true);
            }
        }

        public void Play()
        {
            if (queueCurrent != null)
            {
                queueCurrent.PausePlayback(false);
            }
        }

        public void TogglePause()
        {
            if (queueCurrent != null)
            {
                queueCurrent.PausePlayback(!queueCurrent.Paused);
            }
        }

        public void Skip()
        {
            if (queueCurrent != null)
            {
                queueCurrent.StopPlayback();
            }
        }

        public void Restart()
        {
            if (queueCurrent != null)
            {
                queueCurrent.Restart();
            }
            else    // if we call restart and there is no current clip, we clone the last one we played and add it to the queue again
            {
                if (lastQueuedItem != null)
                {
                    var clone = lastQueuedItem.Clone();
                    clone.addToLog = false;     // we dont want to add this to the log again when we replay it
                    Add(lastQueuedItem);
                }
            }
        }

        private void Update()
        {
            if (queueCurrent == null && queue.Count > 0)
            {
                queueCurrent = queue.Dequeue();
                lastQueuedItem = queueCurrent;
                queueCurrent.StartPlayback();
                if (queueCurrent.addToLog)
                {
                    var alreadyContainsInfo = log.Any(log => log.info.Equals(queueCurrent));
                    if (!alreadyContainsInfo)
                    {
                        AddToLog(queueCurrent);
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Log contains " + log.Count + " items:");
                foreach (var logItem in log)
                {
                    Debug.Log(logItem.info.clip.name + ": " + logItem.info.text);
                }
            }
        }

        private void Enqueue(AudioClipInfo info)
        {
            info.callback = OnFinishPlayback;
            queue.Enqueue(info);
        }

        private void OnFinishPlayback(AudioClipInfo info)
        {
            queueCurrent = null;
        }

        public void AssignAudioMixerGroup(AudioClipInfo info)
        {
            if (info.mixer != null)
                return;

            switch (info.priority)
            {
                case AudioClipPriority.Queue:
                    info.mixer = defaultMixerQueue;
                    break;
                case AudioClipPriority.QueueImmediate:
                    info.mixer = defaultMixerQueueImmediate;
                    break;
                case AudioClipPriority.Background:
                    info.mixer = defaultMixerBackground;
                    break;
                default:
                    Debug.LogError("No mixer group assigned");
                    break;
            }
        }

        private void AddToLog(AudioClipInfo info)
        {
            var logEntry = new AudioLogEntry()
            {
                info = info,
                dateTime = DateTime.Now
            };
            if (onNewAudioLog != null)
                onNewAudioLog.Invoke(logEntry);
        }
    }

    public enum AudioClipPriority
    {
        Queue,              // enqueue, waits for other clips in queue to finish
        QueueImmediate,     // deletes queue and adds itself as first new item
        Background,         // plays immediately without affecting queue or other sounds
    }

    public struct AudioLogEntry
    {
        public AudioClipInfo info;
        public DateTime dateTime;
    }

    [System.Serializable]
    public class NewAudioLogEvent : UnityEvent<AudioLogEntry> { }
}
