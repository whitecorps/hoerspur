﻿namespace Hoerspur.Audio
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Audio;

    public class AudioPlayer : MonoBehaviour
    {
        [Tooltip("This clip gets played as soon as the game starts. This is useful for creating background environment sounds without the need to trigger them first.")]
        public AudioClipInfo onAwake = new AudioClipInfo()
        {
            priority = AudioClipPriority.Background,
            loop = true,
            rolloffMaxDistance = 500,
        };

        private List<AudioClipInfo> sources;

        private void Awake()
        {
            sources = new List<AudioClipInfo>();
            AudioManager.Instance.AssignAudioMixerGroup(onAwake);
            PlayAudio(onAwake);
        }

        public void PlayAudio(AudioClipInfo info)
        {
            if (info.clip != null)
            {
                // create new game object, parent it to player and set its position to player
                GameObject go = new GameObject(info.clip.name);
                go.transform.SetParent(transform);
                go.transform.position = transform.position;
                // add AudioSource to go, feed it neccessary data and play
                AudioSource source = go.AddComponent<AudioSource>();
                source.clip = info.clip;
                source.outputAudioMixerGroup = info.mixer;
                source.loop = info.loop;
                info.audioSourceComponent = source;

                // settings for rolloff, depending on selected calculation and rolloff curve
                switch (info.rolloffCalculation)
                {
                    case AudioClipInfo.RolloffCalculation.None:
                        source.spatialBlend = 0;
                        break;
                    case AudioClipInfo.RolloffCalculation.AttenuationOnly:
                        source.spatialBlend = 0;
                        // volume will be calculated by the Rolloff2D script
                        // this is set to 0 to avoid a "glitch" where the first few milliseconds can be heard before the volume is properly calculated
                        source.volume = 0;
                        var rolloff = source.gameObject.AddComponent<Rolloff2D>();
                        rolloff.rolloffBlend = info.rolloffBlend;
                        rolloff.maxDistance = info.rolloffMaxDistance;
                        break;
                    case AudioClipInfo.RolloffCalculation.AttenuationAndDirection:
                        source.spatialBlend = 1;
                        if (info.rolloffBlend < 0.5f)
                        {
                            source.rolloffMode = AudioRolloffMode.Linear;
                        }
                        else
                        {
                            source.rolloffMode = AudioRolloffMode.Logarithmic;
                        }
                        break;
                }

                source.Play();
                // keep a list of AudioSources for callbacks and cleanup
                sources.Add(info);
            }
        }

        public void StopAll()
        {
            foreach (var ci in sources)
            {
                ci.StopPlayback();
            }
        }

        public void FadeOutAll()
        {
            foreach (var ci in sources)
            {
                StartCoroutine(FadeOut(ci));
            }
        }

        public void FadeOutAll(float duration)
        {
            foreach (var ci in sources)
            {
                StartCoroutine(FadeOut(ci, duration));
            }
        }

        private void OnValidate()
        {
            onAwake.rolloffMaxDistance = Mathf.Max(1f, onAwake.rolloffMaxDistance);
        }

        private IEnumerator FadeOut(AudioClipInfo info, float fadeDuration = 2f)
        {
            if (!info.audioSourceComponent) yield break;
            if (fadeDuration > 0)
            {
                float t = 0f;
                float startVolume = info.audioSourceComponent.volume;
                while (t < 1f)
                {
                    t += Time.deltaTime / fadeDuration;
                    if (!info.audioSourceComponent) yield break;
                    info.audioSourceComponent.volume = Mathf.Lerp(startVolume, 0, t);
                    yield return null;
                }
            }

            if (info.audioSourceComponent)
                info.audioSourceComponent.Stop();
        }

        private void LateUpdate()
        {
            // go over AudioSources, callback those that have finished playing and delete them
            for (int i = sources.Count - 1; i >= 0; i--)
            {
                // only really finished when not playing and not paused
                if (!sources[i].audioSourceComponent.isPlaying && !sources[i].Paused)
                {
                    // destroy go, remove reference to component
                    Destroy(sources[i].audioSourceComponent.gameObject);
                    sources[i].audioSourceComponent = null;
                    // call the callback
                    if (sources[i].callback != null)
                    {
                        sources[i].callback(sources[i]);
                    }
                    // remove from list
                    sources.RemoveAt(i);
                }
            }
        }
    }
}
