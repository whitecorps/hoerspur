﻿namespace Hoerspur.Audio
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Rolloff2D))]
    public class Rolloff2DEditor : Editor
    {
        Rolloff2D rolloff;

        private void OnEnable()
        {
            rolloff = target as Rolloff2D;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (rolloff.audioListener != null)
            {
                var distance = Vector3.Distance(rolloff.transform.position, rolloff.audioListener.transform.position);
                RolloffVisualizer.DrawRolloff(80f, rolloff.maxDistance, rolloff.rolloffBlend, distance);
            }
            else
            {
                RolloffVisualizer.DrawRolloff(80f, rolloff.maxDistance, rolloff.rolloffBlend);
            }
        }
    }
}
