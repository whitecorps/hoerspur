﻿namespace Hoerspur.Variables
{
    using Hoerspur.Utils;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;
    using System.Linq;
    using System;

    [CustomEditor(typeof(VariableManager))]
    public class VariableManagerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorHelper.HeaderLabel("Variables");
            var variables = serializedObject.FindProperty("variables");
            EditorList.Show(variables, 
                EditorListOptions.None,
                EditorListButtonOptions.Move | EditorListButtonOptions.Delete,
                // is called for every list item
                // returns error message strings, which are then displayed
                CheckForVariableError,
                // is called on creating a new list element, clears out any values
                // especially guid which when copied from one variable to another does not work as intended
                ClearVariable,
                DrawVariable
            );
            serializedObject.ApplyModifiedProperties();
        }

        private string CheckForVariableError(SerializedProperty listProperty, int index)
        {
            string name = listProperty.GetArrayElementAtIndex(index).FindPropertyRelative("name").stringValue;
            if (name == "")
                return "Variable needs a name";

            for (int j = 0; j < listProperty.arraySize; j++)
            {
                if ((index != j) && listProperty.GetArrayElementAtIndex(j).FindPropertyRelative("name").stringValue == name)
                {
                    return "'" + name + "' is used more than once";
                }
            }
            return "";
        }

        private void ClearVariable(SerializedProperty variableProperty)
        {
            variableProperty.FindPropertyRelative("name").stringValue = "";
            variableProperty.FindPropertyRelative("valueInt").intValue = 0;
            variableProperty.FindPropertyRelative("valueBool").boolValue = false;
            variableProperty.FindPropertyRelative("type").enumValueIndex = 0;
            variableProperty.FindPropertyRelative("guid").stringValue = Guid.NewGuid().ToString();
        }

        private void DrawVariable(SerializedProperty listProperty, SerializedProperty variableProperty, int index)
        {

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(EditorGUI.indentLevel * EditorHelper.IndentWidth);
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            int oldIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            //if (EditorList.ShowButtons(listProperty, index, ClearVariable)) return;
            EditorGUILayout.PropertyField(variableProperty, GUIContent.none, true);
            EditorList.ShowButtons(listProperty, index, EditorListButtonOptions.All, ClearVariable);
            EditorGUI.indentLevel = oldIndentLevel;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
        }
    }
}
