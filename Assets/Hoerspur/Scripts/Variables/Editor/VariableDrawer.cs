﻿namespace Hoerspur.Variables
{
    using Hoerspur.Utils;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    [CustomPropertyDrawer(typeof(Variable))]
    public class VariableDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            int oldIndentLevel = EditorGUI.indentLevel;
            label = EditorGUI.BeginProperty(position, label, property);
            Rect contentPosition = EditorGUI.PrefixLabel(position, label);
            if (position.height > 16f)
            {
                position.height = 16f;
                EditorGUI.indentLevel += 1;
                contentPosition = EditorGUI.IndentedRect(position);
                contentPosition.y += 18f;
            }
            EditorGUI.indentLevel = 0;

            var typeProperty = property.FindPropertyRelative("type");
            var typeRect = EditorHelper.ResizeRectHorizontal(contentPosition, 0, 0.3f);
            EditorGUI.PropertyField(typeRect, typeProperty, GUIContent.none);

            var nameProperty = property.FindPropertyRelative("name");
            var nameRect = EditorHelper.ResizeRectHorizontal(contentPosition, 0.3f, 0.4f);
            EditorGUI.PropertyField(nameRect, nameProperty, GUIContent.none);

            var valueRect = EditorHelper.ResizeRectHorizontal(contentPosition, 0.7f, 0.3f);
            EditorGUIUtility.labelWidth = 15f;
            switch ((Variable.VariableType)typeProperty.enumValueIndex)
            {
                case Variable.VariableType.Integer:
                    var intProperty = property.FindPropertyRelative("valueInt");
                    EditorGUI.PropertyField(valueRect, intProperty, new GUIContent("\u21d2"));
                    break;
                case Variable.VariableType.Boolean:
                    var boolProperty = property.FindPropertyRelative("valueBool");
                    EditorGUI.PropertyField(valueRect, boolProperty, new GUIContent("\u21d2"));
                    break;
            }

            // error handling
            //if (nameProperty.stringValue == "")
            //{
            //    EditorGUI.DrawRect(contentPosition, new Color(1, 0, 0, 0.25f));
            //}
            EditorGUI.indentLevel = oldIndentLevel;
        }
    }
}