﻿namespace Hoerspur.Variables
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public static class VariableEditorCommon
    {
        public static readonly string[] IntActions = { "=", "+=", "-=" };
        public static readonly string[] BoolActions = { "=", "!" };
        public static readonly string[] VariableTypes = { "(int)", "(bool)" };

        public static readonly string[] IntComparisons = { "==", "!=", ">", ">=", "<", "<=" };
        public static readonly string[] BoolComparisons = { "==", "!=" };
    }
}
