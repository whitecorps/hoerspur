﻿namespace Hoerspur.Variables
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using System.Linq;
    using System;
    using Hoerspur.Utils;

    // TODO: combine this with HoerspurManager
    // this makes it easier to use (no multiple components to handle)
    // and should avoid a lot of errors when no variable manager is in the scene
    public class VariableManager : SingletonSimple<VariableManager>
    {
        // TODO: add context variables (weather)
        // this includes filling these variables with certain values on startup
        // the VariableManager would be responsible for creating and assigning values
        // Q: how to display these in the editor? are they defined during editortime instead of runtime?

        [SerializeField]
        private List<Variable> variables = new List<Variable>();

        public List<Variable> Variables { get { return variables; } }

        public Variable GetByName(string name)
        {
            return variables.FirstOrDefault(v => v.Name == name);
        }

        public Variable GetById(string guid)
        {
            return variables.FirstOrDefault(v => v.Guid == guid);
        }

        public IEnumerable<string> GetNames()
        {
            return variables.Select(v => v.Name);
        }

        public IEnumerable<string> GetIds()
        {
            return variables.Select(v => v.Guid);
        }

        public Variable CreateVariable(string name, Variable.VariableType type)
        {
            var variable = new Variable(name, type);
            variables.Add(variable);
            return variable;
        }
    }
}