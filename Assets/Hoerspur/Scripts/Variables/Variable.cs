﻿namespace Hoerspur.Variables
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class Variable : IComparable<Variable>
    {
        public enum VariableType
        {
            Integer,
            Boolean,
            // Context variable types (enum like?)
            // these context variable types could contain values like "sunny", "rainy", "cloudy" etc.
            // also temperature as int or winddirection as "NW", "S" etc.
        }

        [SerializeField, HideInInspector]
        private string guid = System.Guid.NewGuid().ToString();
        public string Guid { get { return guid; } }

        [SerializeField]
        private VariableType type;
        public VariableType Type { get { return type; } }

        [SerializeField]
        private string name;
        public string Name { get { return name; } }

        [SerializeField]
        private int valueInt;
        public int ValueInt
        {
            get { return valueInt; }
            set { valueInt = value; }
        }

        [SerializeField]
        private bool valueBool;
        public bool ValueBool
        {
            get { return valueBool; }
            set { valueBool = value; }
        }

        public Variable(string name, VariableType type)
        {
            this.name = name;
            this.type = type;
        }

        public int IncrementInt(int by)
        {
            return (valueInt += by);
        }

        public int DecrementInt(int by)
        {
            return (valueInt -= by);
        }

        public int MultiplyInt(int by)
        {
            return (valueInt *= by);
        }

        public bool FlipBool()
        {
            return (valueBool = !valueBool);
        }

        public int CompareTo(Variable other)
        {
            // type of both variables needs to be the same
            if (other.type != type)
                throw new NotSupportedException();

            switch (type)
            {
                case VariableType.Integer:
                    return other.valueInt.CompareTo(valueInt);
                case VariableType.Boolean:
                    return other.valueBool.CompareTo(valueBool);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}