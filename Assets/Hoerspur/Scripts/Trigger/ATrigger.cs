﻿namespace Hoerspur.Triggers
{
    using Hoerspur.Audio;
    using Hoerspur.Geometry;
    using Hoerspur.Utils;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Audio;
    using UnityEngine.Events;

    [RequireComponent(typeof(AGeometry))]
    public abstract class ATrigger : MonoBehaviour
    {
        [SerializeField]
        private ConditionalTarget[] targets;
        [SerializeField, Tooltip("This event is fired when the listener *enters* the trigger area. This event is triggered independently of any targets the user may have defined")]
        private TriggerEvent onTriggerEnter;
        [SerializeField, Tooltip("This event is fired when the listener *exits* the trigger area. This event is triggered independently of any targets the user may have defined")]
        private TriggerEvent onTriggerExit;

        [SerializeField]
        private bool showMeshInGame = true;
        [SerializeField]
        private Material meshMaterial;

        private TimeSince timeSinceActivation;
        public TimeSince TimeSinceActivation { get { return timeSinceActivation; } }

        public abstract bool ContainsWorldPoint(Vector2 point);

        private AGeometry geometry;
        public AGeometry Geometry
        {
            get
            {
                if (geometry == null)
                    geometry = GetComponent<AGeometry>();
                return geometry;
            }
        }

        public Material MeshMaterial
        {
            get { return meshMaterial; }
            set { meshMaterial = value; }
        }

        protected virtual void Awake()
        {
            geometry = GetComponent<AGeometry>();   // just to be safe
        }

        public void Enter()
        {
            timeSinceActivation = 0;
            for (int i = 0; i < targets.Length; i++)
            {
                targets[i].Trigger();
            }

            if (onTriggerEnter != null)
            {
                onTriggerEnter.Invoke();
            }
        }

        public void Exit()
        {
            if (onTriggerExit != null)
            {
                onTriggerExit.Invoke();
            }
        }

        public string[] GetTexts()
        {
            List<string> result = new List<string>();
            foreach (var target in targets)
            {
                if (target.Text != "")
                    result.Add(target.Text);
            }
            return result.ToArray();
        }

        public string GetName()
        {
            if (geometry.JsonId != "")
                return geometry.JsonId;
            else
                return name;
        }
    }

    [System.Serializable]
    public class ConditionalTarget
    {
        [SerializeField, TextArea(4, 20)]
        private string text;

        [SerializeField, Tooltip("The audio clip to be played when this target is triggered.")]
        private AudioClip audioClip;
        [SerializeField, Tooltip("Where (in space) the sound is coming from. This can either be a fixed location or the player object itself (for audio that follows the listener around).")]
        private AudioPlayer audioPlayer;
        [SerializeField, Tooltip("Use this if you have create your own audio mixer within unity you want to use")]
        private AudioMixerGroup mixerOverride;
        [SerializeField, Tooltip("Queue: sound will play after all other currently queued sounds.\n\nQueueImmediate: sound will play immediately, removing all other sounds currently queued.\n\nBackground: sound will play immediately without affecting the queue.")]
        private AudioClipPriority priority;
        [SerializeField, Tooltip("If this is checked, the triggered audio will loop. This is meant to be used with ambient sounds and will probably lead to problems when used with voice-overs.")]
        private bool loop;
        [SerializeField, Tooltip("If this is checked, this target (and its text) will appear in the log, from where it can be replayed.")]
        private bool addToLog = true;
        //[SerializeField, Range(0f, 1f), Tooltip("0: the sound is played \"as is\"\n 1: the sound is played \"in space\", ie. it can be located by turning the head and it gets quieter the further away the listener is.")]
        //private float spatialBlend = 1;
        [SerializeField, Tooltip("None: Sound is 2D, so no rolloff with distance and no way to locate sound in space\n\nAttenuation Only: Sound is 2D, but fades with distance\n\nAttenuation And Direction: Sound is 3D and handled by Unitys spatial audio calculation")]
        private AudioClipInfo.RolloffCalculation rolloffCalculation;
        [Range(0f, 1f), SerializeField, Tooltip("The rolloff the sound should have with distance")]
        private float rolloffBlend;
        [SerializeField, Tooltip("At this distance the volume reaches zero. Only affects linear rolloff.")]
        private float rolloffMaxDistance;

        [SerializeField, Tooltip("If this is checked, this target can only be triggered once. It will not be triggered again, even if all other conditions are met.")]
        private bool once;
        [SerializeField, Tooltip("Define any conditions that have to be met before this target can be triggered.")]
        private List<Condition> conditions;
        [SerializeField, Tooltip("Define any actions that you want to take place as soon as this target is triggered.")]
        private List<Action> actions;

        private bool triggered;

        private bool Triggerable
        {
            get { return !(once && triggered); }
        }

        public string Text
        {
            get { return text; }
        }

        /// <summary>
        /// Evaluates all conditions (AND)
        /// </summary>
        /// <returns>Result of evaluations</returns>
        private bool EvaluateConditions()
        {
            foreach (var condition in conditions)
            {
                // if one condition evaluates to false we return false (AND)
                if (!condition.Evaluate())
                    return false;
            }
            return true;        // no condition evaluated false (or there are none), we return true
        }

        /// <summary>
        /// Executes all actions defined on this target
        /// </summary>
        private void ExecuteActions()
        {
            foreach (var action in actions)
            {
                action.Execute();
            }
        }

        /// <summary>
        /// Triggers the evaluation of the conditions and (if evaluated to true) the execution of the actions
        /// </summary>
        /// <returns>False to prevent triggering the targets following this one</returns>
        public void Trigger()
        {
            if (Triggerable && EvaluateConditions())
            {
                triggered = true;


                // trigger audio stuff
                // audioTarget.Trigger();
                PlayAudio();
                ExecuteActions();
            }
        }

        private void PlayAudio()
        {
            var info = new AudioClipInfo()
            {
                clip = audioClip,
                player = audioPlayer,
                mixer = mixerOverride,
                priority = priority,
                loop = loop,
                addToLog = addToLog,
                text = text,
                rolloffCalculation = rolloffCalculation,
                rolloffMaxDistance = rolloffMaxDistance,
                rolloffBlend = rolloffBlend,
            };
            AudioManager.Instance.Add(info);
        }
    }
    
    [System.Serializable]
    public class TriggerEvent : UnityEvent { }
}