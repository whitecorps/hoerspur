﻿namespace Hoerspur.Triggers
{
    using Hoerspur.Variables;
    using System;
    using UnityEngine;

    [Serializable]
    public class Condition
    {
        public enum ConditionType
        {
            Variable,
            Trigger
        }

        public enum TriggerComparison
        {
            Enabled,
            Timer
        }

        public enum IntComparison
        {
            Equal,
            NotEqual,
            Greater,
            GreaterOrEqual,
            Less,
            LessOrEqual
        }

        public enum TimerComparison
        {
            Less,
            GreaterOrEqual
        }
        
        public ConditionType conditionType;
        [SerializeField]
        private string variableGuid;
        [SerializeField]
        private IntComparison variableComparison;
        [SerializeField]
        private int valueInt;
        [SerializeField]
        private bool valueBool;

        [SerializeField]
        private ATrigger trigger;
        [SerializeField]
        private TriggerComparison triggerComparison;
        [SerializeField]
        private bool valueTriggerEnabled;
        [SerializeField]
        private TimerComparison timerComparison;
        [SerializeField]
        private int valueTriggerTimer;

        public bool Evaluate()
        {
            switch (conditionType)
            {
                case ConditionType.Variable:
                    return EvaluateVariable();
                case ConditionType.Trigger:
                    return EvaluateTrigger();
                default:
                    return true;
            }
        }

        public bool EvaluateVariable()
        {
            var variable = VariableManager.Instance.GetById(variableGuid);
            if (variable == null)
            {
                Debug.LogWarning("Trying to evaluate Variable Condition, but the referenced variable could not be found, ignoring.");
                return true;  // we return true because without a variable to evaluate we consider the condition to not exists, returning false would invalidate all other variable comparisons
            }

            switch (variable.Type)
            {
                case Variable.VariableType.Integer:
                    return EvaluateInt(variable.ValueInt);
                case Variable.VariableType.Boolean:
                    return EvaluateBool(variable.ValueBool);
                default:
                    return true;
            }
        }

        private bool EvaluateInt(int value)
        {
            switch (variableComparison)
            {
                case IntComparison.Equal:
                    return value == valueInt;
                case IntComparison.NotEqual:
                    return value != valueInt;
                case IntComparison.Greater:
                    return value > valueInt;
                case IntComparison.GreaterOrEqual:
                    return value >= valueInt;
                case IntComparison.Less:
                    return value < valueInt;
                case IntComparison.LessOrEqual:
                    return value <= valueInt;
                default:
                    return true;
            }
        }

        private bool EvaluateBool(bool value)
        {
            return value == valueBool;
        }

        private bool EvaluateTrigger()
        {
            if (trigger == null)
            {
                Debug.LogWarning("Trying to evaluate Trigger Condition, but its reference is null, ignoring.");
                return true;
            }
            switch (triggerComparison)
            {
                case TriggerComparison.Enabled:
                    return EvaluateTriggerState();
                case TriggerComparison.Timer:
                    return EvaluateTriggerTimer();
                default:
                    return true;
            }
        }

        private bool EvaluateTriggerState()
        {
            return trigger.enabled == valueTriggerEnabled;
        }

        private bool EvaluateTriggerTimer()
        {
            switch (timerComparison)
            {
                case TimerComparison.Less:
                    return trigger.TimeSinceActivation < valueTriggerTimer;
                default:
                    return trigger.TimeSinceActivation >= valueTriggerTimer;
            }
        }
    }
}
