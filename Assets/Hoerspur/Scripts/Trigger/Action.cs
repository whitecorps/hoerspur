﻿namespace Hoerspur.Triggers
{
    using Hoerspur.Variables;
    using System;
    using UnityEngine;

    [Serializable]
    public class Action {
        public enum ActionType
        {
            Variable,
            Trigger
        }

        public enum VariableAction {
            Set,
            Increment,
            Decrement,
            Flip
        }

        public enum TriggerAction
        {
            SetEnabled,
        }

        [SerializeField]
        private ActionType actionType;

        [SerializeField]
        private VariableAction variableAction;
        [SerializeField]
        private string variableGuid;   
        [SerializeField]
        private int valueInt;
        [SerializeField]
        private bool valueBool;

        [SerializeField]
        private ATrigger trigger;
        [SerializeField]
        private TriggerAction triggerAction;
        [SerializeField]
        private bool triggerEnabled;

        public void Execute()
        {
            switch (actionType)
            {
                case ActionType.Variable:
                    ExecuteVariable();
                    break;
                case ActionType.Trigger:
                    ExecuteTrigger();
                    break;
                default:
                    break;
            }
        }

        private void ExecuteVariable()
        {
            var variable = VariableManager.Instance.GetById(variableGuid);
            if (variable == null)
            {
                Debug.LogWarning("Trying to execute Variable Action, but the referenced variable could not be found, ignoring.");
                return;
            }

            switch (variable.Type)
            {
                case Variable.VariableType.Integer:
                    ExecuteInt(variable);
                    break;
                case Variable.VariableType.Boolean:
                    ExecuteBool(variable);
                    break;
            }
        }

        private void ExecuteInt(Variable variable)
        {
            switch (variableAction)
            {
                case VariableAction.Set:
                    variable.ValueInt = valueInt;
                    break;
                case VariableAction.Increment:
                    variable.IncrementInt(valueInt);
                    break;
                case VariableAction.Decrement:
                    variable.DecrementInt(valueInt);
                    break;
            }
        }

        private void ExecuteBool(Variable variable)
        {
            switch (variableAction)
            {
                case VariableAction.Set:
                    variable.ValueBool = valueBool;
                    break;
                case VariableAction.Flip:
                    variable.FlipBool();
                    break;
            }
        }

        private void ExecuteTrigger()
        {
            if (trigger == null)
            {
                Debug.LogWarning("Trying to execute Trigger Action, but its reference is null, ignoring.");
                return;
            }

            switch (triggerAction)
            {
                case TriggerAction.SetEnabled:
                    trigger.enabled = triggerEnabled;
                    break;
                default:
                    break;
            }
        }
    }
}