﻿namespace Hoerspur.Triggers
{
    using Hoerspur.Geometry;
    using Hoerspur.Utils;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    [RequireComponent(typeof(PolygonGeometry))]
    public class PolygonTrigger : ATrigger
    {
        public override bool ContainsWorldPoint(Vector2 point)
        {
            return Geometry.WorldBounds.Contains(point) &&
                GeoLocationHelper.PolygonContainsPoint(Geometry.WorldCoordinates, point);
        }
    }
}
