﻿namespace Hoerspur.Triggers
{
    using Hoerspur.Geometry;
    using Hoerspur.Utils;
    using UnityEngine;

    [RequireComponent(typeof(PointGeometry))]
    public class PointTrigger : ATrigger
    {
        [SerializeField]
        private float radius;
        [SerializeField, Range(6, 64), Tooltip("This only affects how the trigger looks! For trigger activation only the players' distance to the center is relevant.")]
        private int meshResolution = 32;

        public float Radius
        {
            get { return radius; }
        }

        public int MeshResolution
        {
            get { return meshResolution; }
        }

        public override bool ContainsWorldPoint(Vector2 point)
        {
            return (Vector2.Distance(Geometry.LocalCoordinates[0] + transform.localPosition.AsVector2(), point) < radius);
        }
    }
}