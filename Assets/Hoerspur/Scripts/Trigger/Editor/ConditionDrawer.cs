﻿using Hoerspur.Triggers;
using Hoerspur.Variables;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Condition))]
public class ConditionDrawer : AActionConditionDrawerBase
{
    private static readonly string[] ConditionTypes = Enum.GetNames(typeof(Condition.ConditionType));
    private static readonly string[] IntComparisonTypes = { "=", "≠", ">", "≥", "<", "≤" };
    private static readonly string[] TriggerComparisonTypes = Enum.GetNames(typeof(Condition.TriggerComparison));
    private static readonly string[] TriggerTimerComparisonTypes = { "<", "≥" };

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        int oldIndentLevel = EditorGUI.indentLevel;
        label = EditorGUI.BeginProperty(position, label, property);
        Rect contentPosition = EditorGUI.PrefixLabel(position, label, EditorStyles.foldout).FirstLine();
        EditorGUI.indentLevel = 0;


        // condition type dropdown
        var conditionTypeProp = property.FindPropertyRelative("conditionType");
        DropDownField(contentPosition, conditionTypeProp, ConditionTypes);
        contentPosition = contentPosition.NextLine();

        // depending on condition type we continue with variable/trigger
        var conditionType = (Condition.ConditionType)conditionTypeProp.enumValueIndex;
        switch (conditionType)
        {
            case Condition.ConditionType.Variable:
                VariableGUI(contentPosition, property, label);
                break;
            case Condition.ConditionType.Trigger:
                TriggerGUI(contentPosition, property, label);
                break;
        }
        EditorGUI.indentLevel = oldIndentLevel;
    }

    private void VariableGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // field for variable reference (via guid)
        var varRefProp = property.FindPropertyRelative("variableGuid");
        var selectedVariable = VariableField(position.Select(0f, .4f), varRefProp);

        // if there is a variable selected then
        if (selectedVariable != null)
        {
            // dropdown for comparison (=, >, ...)
            var variableComparisonProp = property.FindPropertyRelative("variableComparison");
            VariableComparisonField(position.Select(.4f, .2f), variableComparisonProp, selectedVariable);

            // field for variable value to compare against
            VariableValueField(position.Select(.6f, .4f), property, selectedVariable);
        }
    }

    private void VariableComparisonField(Rect position, SerializedProperty property, Variable variable)
    {
        switch (variable.Type)
        {
            case Variable.VariableType.Integer:
                // dropdown for number comparisons
                DropDownField(position, property, IntComparisonTypes);
                break;
            case Variable.VariableType.Boolean:
                // boolean only has equals, so we use a label
                EditorGUI.LabelField(position, "=");
                break;
        }
    }

    private void VariableValueField(Rect position, SerializedProperty property, Variable variable)
    {
        switch (variable.Type)
        {
            case Variable.VariableType.Integer:
                // show an int field if variable is an int
                var valueIntProp = property.FindPropertyRelative("valueInt");
                valueIntProp.intValue = EditorGUI.IntField(position, valueIntProp.intValue);
                break;
            case Variable.VariableType.Boolean:
                // show a bool field if variable is a bool
                var valueBoolProp = property.FindPropertyRelative("valueBool");
                valueBoolProp.boolValue = EditorGUI.Toggle(position, valueBoolProp.boolValue);
                break;
        }
    }

    private void TriggerGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // reference field for trigger
        var triggerRefProp = property.FindPropertyRelative("trigger");
        EditorGUI.PropertyField(position.Select(0f, .4f), triggerRefProp, GUIContent.none);

        // dropdown for trigger comparison (enabled?, timer)
        var triggerComparisonProp = property.FindPropertyRelative("triggerComparison");
        DropDownField(position.Select(.4f, .2f), triggerComparisonProp, TriggerComparisonTypes);

        var triggerComparison = (Condition.TriggerComparison)triggerComparisonProp.enumValueIndex;
        switch (triggerComparison)
        {
            case Condition.TriggerComparison.Enabled:
                // show bool value field for enabled check
                var valueTriggerEnabledProp = property.FindPropertyRelative("valueTriggerEnabled");
                EditorGUI.PropertyField(position.Select(0.6f, 0.4f), valueTriggerEnabledProp, GUIContent.none);
                break;
            case Condition.TriggerComparison.Timer:
                // show comparison for timer value (only < and >=)
                var timerComparisonProp = property.FindPropertyRelative("timerComparison");
                DropDownField(position.Select(0.6f, 0.2f), timerComparisonProp, TriggerTimerComparisonTypes);
                // show int field for value to compare the timer against
                var valueTriggerTimerProp = property.FindPropertyRelative("valueTriggerTimer");
                EditorGUI.PropertyField(position.Select(0.8f, 0.2f), valueTriggerTimerProp, GUIContent.none);
                break;
        }
    }
}
