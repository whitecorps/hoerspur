﻿//namespace Hoerspur.Variables
//{
//    using Hoerspur.Utils;
//    using System.Collections;
//    using System.Collections.Generic;
//    using UnityEditor;
//    using UnityEngine;

//    [CustomPropertyDrawer(typeof(VariableCondition))]
//    public class VariableConditionDrawer : PropertyDrawer
//    {
//        string labelText;

//        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//        {
//            var varRefProperty = property.FindPropertyRelative("variableGuid");
//            var comparisonProperty = property.FindPropertyRelative("comparison");

//            BuildLabelText(varRefProperty.stringValue, (VariableCondition.VariableComparison)comparisonProperty.enumValueIndex, property);
//            if (labelText != "" && label.text != "")
//                label.text = labelText;
//            labelText = "";

//            int oldIndentLevel = EditorGUI.indentLevel;

//            label = EditorGUI.BeginProperty(position, label, property);
//            Rect contentPosition = EditorGUI.PrefixLabel(position, label);
//            if (position.height > 16f)
//            {
//                position.height = 16f;
//                EditorGUI.indentLevel += 1;
//                contentPosition = EditorGUI.IndentedRect(position);
//                contentPosition.y += 18f;
//            }
//            EditorGUI.indentLevel = 0;

//            DrawVariablePopup(EditorHelper.ResizeRectHorizontal(contentPosition, 0f, 0.4f), varRefProperty);
//            if (varRefProperty.stringValue != null && varRefProperty.stringValue != "")
//            {
//                DrawComparisonPopup(EditorHelper.ResizeRectHorizontal(contentPosition, 0.4f, 0.2f), comparisonProperty, varRefProperty.stringValue);
//                DrawValueField(EditorHelper.ResizeRectHorizontal(contentPosition, 0.6f, 0.4f), property, varRefProperty.stringValue);
//            }

//            EditorGUI.indentLevel = oldIndentLevel;
//            EditorGUI.EndProperty();
//        }

//        private void DrawVariablePopup(Rect position, SerializedProperty varRefProperty)
//        {
//            // variable by guid reference
//            var popupValues = new List<string>();
//            var references = new List<string>();
//            var selectedIndex = 0;
//            var variables = VariableManager.Instance.Variables;
//            // TODO: besseres handling mit anzeige "keine variablen definiert"?
//            for (int i = 0; i < variables.Count; i++)
//            {
//                var variable = variables[i];
//                popupValues.Add(VariableEditorCommon.VariableTypes[(int)variable.Type] + " " + variable.Name);
//                references.Add(variable.Guid);
//                if (variable.Guid == varRefProperty.stringValue)
//                    selectedIndex = i;
//            }
//            selectedIndex = EditorGUI.Popup(position, selectedIndex, popupValues.ToArray());
//            if (references.Count > 0)
//            {
//                varRefProperty.stringValue = references[selectedIndex];
//            }
//            else
//                varRefProperty.stringValue = "";
//        }

//        private void DrawComparisonPopup(Rect position, SerializedProperty comparisonProperty, string varRef)
//        {
//            var variable = VariableManager.Instance.GetById(varRef);
//            int selectedIndex = comparisonProperty.enumValueIndex;
//            switch (variable.Type)
//            {
//                case Variable.VariableType.Integer:
//                    selectedIndex = EditorGUI.Popup(position, selectedIndex, VariableEditorCommon.IntComparisons);
//                    break;
//                case Variable.VariableType.Boolean:
//                    if (selectedIndex > 1)
//                        selectedIndex = 0;
//                    selectedIndex = EditorGUI.Popup(position, selectedIndex, VariableEditorCommon.BoolComparisons);
//                    break;
//            }
//            comparisonProperty.enumValueIndex = selectedIndex;
//        }

//        private void DrawValueField(Rect position, SerializedProperty property, string varRef)
//        {
//            var variable = VariableManager.Instance.GetById(varRef);
//            //EditorGUIUtility.labelWidth = 14f;
//            switch (variable.Type)
//            {
//                case Variable.VariableType.Integer:
//                    var valueIntProperty = property.FindPropertyRelative("valueInt");
//                    EditorGUI.PropertyField(position, valueIntProperty, GUIContent.none);
//                    break;
//                case Variable.VariableType.Boolean:
//                    var valueBoolProperty = property.FindPropertyRelative("valueBool");
//                    EditorGUI.PropertyField(position, valueBoolProperty, GUIContent.none);
//                    break;
//            }
//        }

//        private void BuildLabelText(string varRef, VariableCondition.VariableComparison comparison, SerializedProperty property)
//        {
//            var variable = VariableManager.Instance.GetById(varRef);
//            if (variable == null)
//                return;
//            labelText = variable.Name;
//            switch (comparison)
//            {
//                case VariableCondition.VariableComparison.Equal:
//                    labelText += " ==";
//                    break;
//                case VariableCondition.VariableComparison.NotEqual:
//                    labelText += " !=";
//                    break;
//                case VariableCondition.VariableComparison.Greater:
//                    labelText += " >";
//                    break;
//                case VariableCondition.VariableComparison.GreaterOrEqual:
//                    labelText += " >=";
//                    break;
//                case VariableCondition.VariableComparison.Less:
//                    labelText += " <";
//                    break;
//                case VariableCondition.VariableComparison.LessOrEqual:
//                    labelText += " <=";
//                    break;
//                default:
//                    break;
//            }
//            switch (variable.Type)
//            {
//                case Variable.VariableType.Integer:
//                    labelText += " " + property.FindPropertyRelative("valueInt").intValue;
//                    break;
//                case Variable.VariableType.Boolean:
//                    labelText += " " + property.FindPropertyRelative("valueBool").boolValue;
//                    break;
//                default:
//                    break;
//            }
//        }

//        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
//        {
//            return label != GUIContent.none && Screen.width < 333 ? (16f + 18f) : 16f;
//        }
//    }
//}
