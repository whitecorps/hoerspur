﻿namespace Hoerspur.Triggers
{
    using Hoerspur.Utils;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(PointTrigger))]
    public class PointTriggerEditor : ATriggerEditor
    {
        //private const int CircleResolution = 32;
        private const float MinRadius = 5f;

        private SerializedProperty radiusProp;
        private SerializedProperty meshResolutionProp;

        protected override void OnEnable()
        {
            base.OnEnable();
            radiusProp = serializedObject.FindProperty("radius");
            meshResolutionProp = serializedObject.FindProperty("meshResolution");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();
            EditorGUILayout.PropertyField(meshResolutionProp);
            EditorGUILayout.Separator();
            EditorHelper.HeaderLabel("Point Settings");
            EditorGUILayout.PropertyField(radiusProp);
            if (radiusProp.floatValue < MinRadius) radiusProp.floatValue = MinRadius;
            if (serializedObject.ApplyModifiedProperties())
            {
                UpdateMesh();
            }
        }

        public override void UpdateMesh()
        {
            var verts = GenerateCircleVertices(radiusProp.floatValue, meshResolutionProp.intValue, Vector2.zero);
            UpdateCircleMesh(verts);
        }

        private static List<Vector2> GenerateCircleVertices(float radius, int resolution, Vector2 offset)
        {
            var verts = new List<Vector2>();
            for (int i = 0; i < resolution; i++)
            {
                var t = Mathf.PI * 2f * i / resolution;
                verts.Add(new Vector2(Mathf.Cos(t), Mathf.Sin(t)) * radius + offset);
            }
            return verts;
        }

        private void UpdateCircleMesh(List<Vector2> verts)
        {
            var mesh = GetMesh();

            mesh.vertices = Array.ConvertAll(verts.ToArray(), p => new Vector3(p.x, p.y, MeshOffset)); // Vector2 -> Vector3
            mesh.triangles = Triangulator.Triangulate(verts);
            mesh.name = trigger.name + "_Circle";
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            trigger.GetComponent<MeshRenderer>().material = trigger.MeshMaterial;
        }

        [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected | GizmoType.Active)]
        static void DrawPointGeometryGizmo(PointTrigger trigger, GizmoType gizmoType)
        {
            bool active = ((gizmoType & GizmoType.Active) != 0);
            var center = trigger.Geometry.WorldCoordinates[0];
            Gizmos.color = active ? EditorHelper.GizmoLineColorActive : EditorHelper.GizmoLineColor;
            var coords = GenerateCircleVertices(trigger.Radius, trigger.MeshResolution, center);
            for (int i = 0; i < coords.Count - 1; i++)
            {
                Gizmos.DrawLine(coords[i], coords[i + 1]);
            }
            if (coords.Count > 2)
            {
                Gizmos.DrawLine(coords[coords.Count - 1], coords[0]);
            }
        }
    }
}