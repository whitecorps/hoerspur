﻿namespace Hoerspur.Triggers
{
    using Hoerspur.Variables;
    using System;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    [CustomPropertyDrawer(typeof(Action))]
    public class ActionDrawer : AActionConditionDrawerBase
    {
        private static readonly string[] ActionTypes = Enum.GetNames(typeof(Action.ActionType));
        private static readonly string[] IntActionTypes = { "=", "+=", "-=" };
        private static readonly string[] BoolActionTypes = { "=", "flip" };
        private static readonly string[] TriggerActionTypes = Enum.GetNames(typeof(Action.TriggerAction));

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            int oldIndentLevel = EditorGUI.indentLevel;
            label = EditorGUI.BeginProperty(position, label, property);
            Rect contentPosition = EditorGUI.PrefixLabel(position, label).FirstLine();
            EditorGUI.indentLevel = 0;

            // dropdown for action type (variable or trigger)
            var actionTypeProp = property.FindPropertyRelative("actionType");
            DropDownField(contentPosition, actionTypeProp, ActionTypes);
            contentPosition = contentPosition.NextLine();

            // depending on action type selection we show gui for variable or trigger
            var actionType = (Action.ActionType)actionTypeProp.enumValueIndex;
            switch (actionType)
            {
                case Action.ActionType.Variable:
                    VariableGUI(contentPosition, property, label);
                    break;
                case Action.ActionType.Trigger:
                    TriggerGUI(contentPosition, property, label);
                    break;
            }

            EditorGUI.indentLevel = oldIndentLevel;
        }

        #region Variable specific fields
        private void VariableGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // show dropdown for variable selection
            var varRefProp = property.FindPropertyRelative("variableGuid");
            var selectedVariable = VariableField(position.Select(0f, .4f), varRefProp);

            // if a variable is selected,...
            if (selectedVariable != null)
            {
                // show dropdown for what to do with variable (set, increase,...)
                var varActionType = property.FindPropertyRelative("variableAction");
                VariableActionField(position.Select(.4f, .2f), varActionType, selectedVariable);

                // if var action type is not set to "flip" then show a value field
                // if type is "flip" no further input is required
                if (varActionType.enumValueIndex != 3)
                    VariableValueField(position.Select(.6f, .4f), property, selectedVariable);
            }
        }

        private void VariableActionField(Rect position, SerializedProperty property, Variable variable)
        {
            var selectedIndex = property.enumValueIndex;
            switch (variable.Type)
            {
                case Variable.VariableType.Integer:
                    // index 3 is "flip", makes no sense for an int
                    if (selectedIndex == 3) selectedIndex = 0;
                    selectedIndex = EditorGUI.Popup(position, selectedIndex, IntActionTypes);
                    break;
                case Variable.VariableType.Boolean:
                    // if index is 3 we set it to 1, as that is its position in the BoolActionTypes string array
                    // set it to 0 otherwise as all other options are for ints only
                    if (selectedIndex == 3)
                        selectedIndex = 1;
                    else
                        selectedIndex = 0;
                    selectedIndex = EditorGUI.Popup(position, selectedIndex, BoolActionTypes);
                    // set index back to 3, as BoolActionTypes[1] equals VariableAction[3]
                    if (selectedIndex == 1) selectedIndex = 3;
                    break;
            }
            property.enumValueIndex = selectedIndex;
        }

        private void VariableValueField(Rect position, SerializedProperty property, Variable variable)
        {
            switch (variable.Type)
            {
                case Variable.VariableType.Integer:
                    // show value field for int
                    var valueIntProp = property.FindPropertyRelative("valueInt");
                    valueIntProp.intValue = EditorGUI.IntField(position, valueIntProp.intValue);
                    break;
                case Variable.VariableType.Boolean:
                    // show value field for bool
                    var valueBoolProp = property.FindPropertyRelative("valueBool");
                    valueBoolProp.boolValue = EditorGUI.Toggle(position, valueBoolProp.boolValue);
                    break;
            }
        }
        #endregion

        #region Trigger specific 
        private void TriggerGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // show property field for trigger
            var triggerProp = property.FindPropertyRelative("trigger");
            EditorGUI.PropertyField(position.Select(0f, .4f), triggerProp, GUIContent.none);

            // show dropdown for trigger action (currently only one)
            var triggerActionProp = property.FindPropertyRelative("triggerAction");
            DropDownField(position.Select(.4f, .2f), triggerActionProp, TriggerActionTypes);

            // show property field for trigger enabled
            // this is unconditional because there is only one trigger action to chose from
            // this may change at a later date
            var triggerEnabledProp = property.FindPropertyRelative("triggerEnabled");
            EditorGUI.PropertyField(position.Select(.6f, .4f), triggerEnabledProp, GUIContent.none);
        }
        #endregion
    }
}