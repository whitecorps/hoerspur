﻿namespace Hoerspur.Triggers
{
    using Hoerspur.Utils;
    using System;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(PolygonTrigger))]
    public class PolygonTriggerEditor : ATriggerEditor
    {
        //[DrawGizmo(GizmoType.NonSelected | GizmoType.Selected | GizmoType.Active)]
        //private static void DrawPolygonTriggerGizmo(PolygonTrigger trigger, GizmoType gizmoType)
        //{
        //    bool active = ((gizmoType & GizmoType.Active) != 0);

        //    Handles.color = active ? EditorHelper.GizmoColorActive : EditorHelper.GizmoColor;

        //    var coords = trigger.Geometry.WorldCoordinates;
        //    Vector3[] verts = new Vector3[coords.Count];
        //    for (var i = 0; i < coords.Count; i++)
        //    {
        //        verts[i] = coords[i];
        //    }
        //    Handles.DrawAAConvexPolygon(verts);
        //}


        public override void UpdateMesh()
        {
            var mesh = GetMesh();

            mesh.vertices = Array.ConvertAll(trigger.Geometry.LocalCoordinates.ToArray(), p => new Vector3(p.x, p.y, MeshOffset)); // Vector2 -> Vector3
            mesh.triangles = Triangulator.Triangulate(trigger.Geometry.LocalCoordinates);
            mesh.name = trigger.name + "_Polygon";
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            trigger.GetComponent<MeshRenderer>().material = trigger.MeshMaterial;
        }
    }
}