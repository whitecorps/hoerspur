﻿//namespace Hoerspur.Variables
//{
//    using Hoerspur.Triggers;
//    using Hoerspur.Utils;
//    using System.Collections.Generic;
//    using UnityEditor;
//    using UnityEngine;

//    [CustomPropertyDrawer(typeof(Action))]
//    public class VariableActionDrawer : PropertyDrawer
//    {
//        private string labelText;

//        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//        {
//            var varRefProperty = property.FindPropertyRelative("variableGuid");
//            var typeProperty = property.FindPropertyRelative("type");

//            BuildLabelText(varRefProperty.stringValue, (Action.ActionType)typeProperty.enumValueIndex, property);
//            if (labelText != "" && label.text != "")
//                label.text = labelText;
//            labelText = "";

//            int oldIndentLevel = EditorGUI.indentLevel;
//            label = EditorGUI.BeginProperty(position, label, property);

//            Rect contentPosition = EditorGUI.PrefixLabel(position, label);
//            if (position.height > 16f)
//            {
//                position.height = 16f;
//                EditorGUI.indentLevel += 1;
//                contentPosition = EditorGUI.IndentedRect(position);
//                contentPosition.y += 18f;
//            }
//            EditorGUI.indentLevel = 0;

//            // variable reference
//            DrawVariablePopup(EditorHelper.ResizeRectHorizontal(contentPosition, 0, 0.4f), varRefProperty);

//            if (varRefProperty.stringValue != null && varRefProperty.stringValue != "")
//            {
//                // action type
//                DrawTypePopup(EditorHelper.ResizeRectHorizontal(contentPosition, 0.4f, 0.2f), typeProperty, varRefProperty.stringValue);

//                // value field
//                DrawValueField(EditorHelper.ResizeRectHorizontal(contentPosition, 0.6f, 0.4f), property, (VariableAction.ActionType)typeProperty.enumValueIndex, varRefProperty.stringValue);
//            }

//            EditorGUI.EndProperty();
//            EditorGUI.indentLevel = oldIndentLevel;
//        }

//        private void DrawVariablePopup(Rect position, SerializedProperty varRefProperty)
//        {
//            // variable by guid reference
//            var popupValues = new List<string>();
//            var references = new List<string>();
//            var selectedIndex = 0;
//            var variables = VariableManager.Instance.Variables;
//            // TODO: besseres handling mit anzeige "keine variablen definiert"?
//            for (int i = 0; i < variables.Count; i++)
//            {
//                var variable = variables[i];
//                popupValues.Add(VariableEditorCommon.VariableTypes[(int)variable.Type] + " " + variable.Name);
//                references.Add(variable.Guid);
//                if (variable.Guid == varRefProperty.stringValue)
//                    selectedIndex = i;
//            }
//            selectedIndex = EditorGUI.Popup(position, selectedIndex, popupValues.ToArray());
//            if (references.Count > 0)
//            {
//                varRefProperty.stringValue = references[selectedIndex];
//            }
//            else
//                varRefProperty.stringValue = "";
//        }

//        private void DrawTypePopup(Rect position, SerializedProperty typeProperty, string varRef)
//        {
//            var variable = VariableManager.Instance.GetById(varRef);
//            int selectedIndex = typeProperty.enumValueIndex;
//            switch (variable.Type)
//            {
//                case Variable.VariableType.Integer:
//                    if (selectedIndex > 2) selectedIndex = 0;
//                    selectedIndex = EditorGUI.Popup(position, selectedIndex, VariableEditorCommon.IntActions);
//                    break;
//                case Variable.VariableType.Boolean:
//                    if (selectedIndex == 3)
//                        selectedIndex = 1;
//                    else
//                        selectedIndex = 0;
//                    selectedIndex = EditorGUI.Popup(position, selectedIndex, VariableEditorCommon.BoolActions);
//                    if (selectedIndex == 1) selectedIndex = 3;
//                    break;
//            }
//            typeProperty.enumValueIndex = selectedIndex;
//        }

//        private void DrawValueField(Rect position, SerializedProperty property, VariableAction.ActionType actionType, string varRef)
//        {
//            var variable = VariableManager.Instance.GetById(varRef);
//            //EditorGUIUtility.labelWidth = 14f;
//            switch (variable.Type)
//            {
//                case Variable.VariableType.Integer:
//                    var valueIntProperty = property.FindPropertyRelative("valueInt");
//                    EditorGUI.PropertyField(position, valueIntProperty, GUIContent.none);
//                    break;
//                case Variable.VariableType.Boolean:
//                    if (actionType != VariableAction.ActionType.Flip)
//                    {
//                        var valueBoolProperty = property.FindPropertyRelative("valueBool");
//                        EditorGUI.PropertyField(position, valueBoolProperty, GUIContent.none);
//                    }
//                    break;
//            }
//        }

//        private void BuildLabelText(string varRef, VariableAction.ActionType type, SerializedProperty property)
//        {
//            var variable = VariableManager.Instance.GetById(varRef);
//            if (variable == null)
//                return;
//            labelText = variable.Name;
//            switch (type)
//            {
//                case VariableAction.ActionType.Set:
//                    labelText += " =";
//                    break;
//                case VariableAction.ActionType.Increment:
//                    labelText += " = " + variable.Name + " +";
//                    break;
//                case VariableAction.ActionType.Decrement:
//                    labelText += " = " + variable.Name + " -";
//                    break;
//                case VariableAction.ActionType.Flip:
//                    labelText += " = !" + variable.Name;
//                    break;
//                default:
//                    break;
//            }
//            switch (variable.Type)
//            {
//                case Variable.VariableType.Integer:
//                    labelText += " " + property.FindPropertyRelative("valueInt").intValue;
//                    break;
//                case Variable.VariableType.Boolean:
//                    if (type != VariableAction.ActionType.Flip)
//                        labelText += " " + property.FindPropertyRelative("valueBool").boolValue;
//                    break;
//                default:
//                    break;
//            }
//        }

//        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
//        {
//            return label != GUIContent.none && Screen.width < 333 ? (16f + 18f) : 16f;
//        }
//    }
//}