﻿namespace Hoerspur.Triggers
{
    using Hoerspur.Utils;
    using System;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(ATrigger), true)]
    public class ATriggerEditor : Editor
    {
        private const int LabelStringLength = 45;
        protected const float MeshOffset = -1f;

        protected ATrigger trigger;

        private SerializedProperty showMeshInGameProp;
        private SerializedProperty meshMaterialProp;
        private SerializedProperty triggerEnterProp;
        private SerializedProperty triggerExitProp;

        protected virtual void OnEnable()
        {
            trigger = target as ATrigger;

            showMeshInGameProp = serializedObject.FindProperty("showMeshInGame");
            meshMaterialProp = serializedObject.FindProperty("meshMaterial");
            triggerEnterProp = serializedObject.FindProperty("onTriggerEnter");
            triggerExitProp = serializedObject.FindProperty("onTriggerExit");

            Undo.undoRedoPerformed += UndoRedoCallback;
        }

        protected virtual void OnDisable()
        {
            Undo.undoRedoPerformed -= UndoRedoCallback;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorHelper.HeaderLabel("Trigger Settings");

            var targets = serializedObject.FindProperty("targets");
            EditorList.Show(targets, EditorListOptions.ListLabel, singlePropertyDrawer: SingleTargetDrawer, onCreate: ClearTargetProperty);


            EditorHelper.HeaderLabel("Trigger Events");
            triggerEnterProp.isExpanded = EditorGUILayout.Foldout(triggerEnterProp.isExpanded, "Enter/Exit");
            if (triggerEnterProp.isExpanded)
            {
                EditorGUILayout.PropertyField(triggerEnterProp);
                EditorGUILayout.PropertyField(triggerExitProp);
            }

            EditorHelper.HeaderLabel("Mesh settings");
            EditorGUILayout.PropertyField(showMeshInGameProp);
            if (showMeshInGameProp.boolValue)
            {
                EditorGUILayout.PropertyField(meshMaterialProp);
                SetMaterialIfNull();
            }

            if (serializedObject.ApplyModifiedProperties())
            {
                UpdateMesh(showMeshInGameProp.boolValue);
            }
        }

        private void SingleTargetDrawer(SerializedProperty list, SerializedProperty listItem, int index)
        {
            var labelText = CreateLabelString(listItem);
            EditorGUILayout.BeginHorizontal();
            {
                GUILayout.Space(EditorGUI.indentLevel * EditorHelper.IndentWidth);
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        listItem.isExpanded = EditorGUILayout.Foldout(listItem.isExpanded, labelText);
                        if (EditorList.ShowButtons(list, index, EditorListButtonOptions.NoDuplicate, ClearTargetProperty))
                            return;
                    }
                    EditorGUILayout.EndHorizontal();

                    if (listItem.isExpanded)
                    {
                        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                        {
                            var textProperty = listItem.FindPropertyRelative("text");
                            textProperty.isExpanded = EditorGUILayout.Foldout(textProperty.isExpanded, "Settings", true);
                            if (textProperty.isExpanded)
                            {
                                EditorGUILayout.PropertyField(listItem.FindPropertyRelative("text"));
                                EditorGUILayout.Separator();

                                EditorGUILayout.LabelField("Audio", EditorStyles.boldLabel);
                                EditorGUILayout.PropertyField(listItem.FindPropertyRelative("audioClip"));
                                EditorGUILayout.PropertyField(listItem.FindPropertyRelative("audioPlayer"));
                                EditorGUILayout.PropertyField(listItem.FindPropertyRelative("mixerOverride"));

                                var priorityProperty = listItem.FindPropertyRelative("priority");
                                EditorGUILayout.PropertyField(priorityProperty);

                                // we only want to be able to loop background sounds, so we display the option only when the user has selected Background priority
                                var loopProperty = listItem.FindPropertyRelative("loop");
                                if (priorityProperty.enumValueIndex == 2)
                                {
                                    EditorGUILayout.PropertyField(loopProperty);
                                }
                                else
                                {
                                    // set to false in case anything other than Background is selected asd priority
                                    loopProperty.boolValue = false;
                                }
                                //EditorGUILayout.PropertyField(listItem.FindPropertyRelative("spatialBlend"));
                                var rolloffCalculationProp = listItem.FindPropertyRelative("rolloffCalculation");
                                EditorGUILayout.PropertyField(rolloffCalculationProp);
                                if (rolloffCalculationProp.enumValueIndex == 1)     // falloff enabled
                                {
                                    var rolloffBlendProp = listItem.FindPropertyRelative("rolloffBlend");
                                    EditorGUILayout.PropertyField(rolloffBlendProp);
                                    var rolloffMaxDistanceProp = listItem.FindPropertyRelative("rolloffMaxDistance");
                                    EditorGUILayout.PropertyField(rolloffMaxDistanceProp);
                                    if (rolloffMaxDistanceProp.floatValue < 1)
                                    {
                                        rolloffMaxDistanceProp.floatValue = 1;
                                    }

                                    RolloffVisualizer.DrawRolloff(80f, rolloffMaxDistanceProp.floatValue, rolloffBlendProp.floatValue);
                                }
                                else if (rolloffCalculationProp.enumValueIndex == 2)
                                {
                                    var rolloffBlendProp = listItem.FindPropertyRelative("rolloffBlend");
                                    rolloffBlendProp.floatValue = EditorGUILayout.IntSlider("Rolloff Blend", Mathf.RoundToInt(rolloffBlendProp.floatValue), 0, 1);
                                    var rolloffMaxDistanceProp = listItem.FindPropertyRelative("rolloffMaxDistance");
                                    EditorGUILayout.PropertyField(rolloffMaxDistanceProp);
                                    RolloffVisualizer.DrawRolloff(80f, rolloffMaxDistanceProp.floatValue, rolloffBlendProp.floatValue);
                                }

                                EditorGUILayout.Separator();

                                EditorGUILayout.LabelField("Other", EditorStyles.boldLabel);
                                EditorGUILayout.PropertyField(listItem.FindPropertyRelative("once"));
                                // we only want to be able to add targets to the log that are not Background priority sounds, so we only display the option when
                                // the user has selected either Queue or QueueImmediate priority
                                var addToLogProperty = listItem.FindPropertyRelative("addToLog");
                                if (priorityProperty.enumValueIndex != 2)
                                {
                                    EditorGUILayout.PropertyField(addToLogProperty);
                                }
                                else
                                {
                                    // set to false in case Background is selected as priority
                                    addToLogProperty.boolValue = false;
                                }
                            }
                        }
                        EditorGUILayout.EndVertical();

                        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                        {
                            var conditionsProperty = listItem.FindPropertyRelative("conditions");
                            conditionsProperty.isExpanded = EditorGUILayout.Foldout(conditionsProperty.isExpanded, string.Format("Conditions ({0})", conditionsProperty.arraySize), true);
                            if (conditionsProperty.isExpanded)
                            {
                                var vm = Variables.VariableManager.Instance;
                                //if (vm != null && vm.Variables.Count > 0)
                                //{
                                EditorGUILayout.Separator();
                                EditorList.Show(conditionsProperty, EditorListOptions.Buttons | EditorListOptions.BoxedElements, EditorListButtonOptions.NoDuplicate);
                                //}
                                //else
                                //{
                                //    EditorGUILayout.HelpBox("No variables defined", MessageType.Info);
                                //}
                            }
                        }
                        EditorGUILayout.EndVertical();

                        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                        {
                            var actionsProperty = listItem.FindPropertyRelative("actions");
                            actionsProperty.isExpanded = EditorGUILayout.Foldout(actionsProperty.isExpanded, string.Format("Actions ({0})", actionsProperty.arraySize), true);
                            if (actionsProperty.isExpanded)
                            {
                                var vm = Variables.VariableManager.Instance;
                                //if (vm != null && vm.Variables.Count > 0)
                                //{
                                EditorGUILayout.Separator();
                                EditorList.Show(actionsProperty, EditorListOptions.Buttons | EditorListOptions.BoxedElements, EditorListButtonOptions.NoDuplicate);
                                //}
                                //else
                                //{
                                //    EditorGUILayout.HelpBox("No variables defined", MessageType.Info);
                                //}
                            }
                        }
                        EditorGUILayout.EndVertical();
                    }
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
        }

        private void ClearTargetProperty(SerializedProperty targetProperty)
        {
            targetProperty.FindPropertyRelative("text").stringValue = "";
            targetProperty.FindPropertyRelative("audioPlayer").objectReferenceValue = null;
            targetProperty.FindPropertyRelative("audioClip").objectReferenceValue = null;
            targetProperty.FindPropertyRelative("mixerOverride").objectReferenceValue = null;
            targetProperty.FindPropertyRelative("priority").enumValueIndex = 0;
            targetProperty.FindPropertyRelative("once").boolValue = true;
            targetProperty.FindPropertyRelative("conditions").ClearArray();
            targetProperty.FindPropertyRelative("actions").ClearArray();
            targetProperty.FindPropertyRelative("rolloffMaxDistance").floatValue = 500;
            targetProperty.isExpanded = true;
        }

        private string CreateLabelString(SerializedProperty targetProperty)
        {
            var labelString = targetProperty.FindPropertyRelative("text").stringValue;
            if (labelString == "") return "(no text)";
            labelString = labelString.Split('\n')[0] + "...";
            labelString = labelString.Length > LabelStringLength ? labelString.Substring(0, LabelStringLength) + "..." : labelString;
            return labelString;
        }

        protected void RemoveMesh()
        {
            var mr = trigger.GetComponent<MeshRenderer>();
            if (mr) DestroyImmediate(mr);
            var mf = trigger.GetComponent<MeshFilter>();
            if (mf) DestroyImmediate(mf);
        }

        private void SetMaterialIfNull()
        {
            if (meshMaterialProp.objectReferenceValue == null)
            {
                var guid = AssetDatabase.FindAssets("PolyTriggerMaterial")[0];
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var material = AssetDatabase.LoadAssetAtPath<Material>(path);
                meshMaterialProp.objectReferenceValue = material;
            }
        }

        protected Mesh GetMesh()
        {
            var mf = trigger.GetComponent<MeshFilter>();
            if (!mf)
                mf = trigger.gameObject.AddComponent<MeshFilter>();

            if (mf.sharedMesh == null)
                mf.sharedMesh = new Mesh();
            else
                mf.sharedMesh.Clear();

            var mr = trigger.GetComponent<MeshRenderer>();
            if (!mr)
                mr = trigger.gameObject.AddComponent<MeshRenderer>();

            return mf.sharedMesh;
        }

        public void UpdateOrRemoveMesh()
        {
            UpdateMesh(showMeshInGameProp.boolValue);
        }

        public void UpdateMesh(bool showMesh)
        {
            if (showMesh)
                UpdateMesh();
            else
                RemoveMesh();
        }

        public virtual void UpdateMesh()
        {
            throw new NotImplementedException();
        }

        private void UndoRedoCallback()
        {
            serializedObject.Update();
            UpdateOrRemoveMesh();
        }
    }
}