﻿using Hoerspur.Variables;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public abstract class AActionConditionDrawerBase : PropertyDrawer
{
    protected Variable VariableField(Rect position, SerializedProperty property)
    {
        var displayValues = new List<string>();
        var referenceValues = new List<string>();
        var variables = VariableManager.Instance.Variables;
        var selectedIndex = 0;
        if (variables.Count == 0)
        {
            displayValues.Add("No variables defined");
        }
        else
        {
            for (int i = 0; i < variables.Count; i++)
            {
                displayValues.Add(GetVariableDisplayValue(variables[i]));
                referenceValues.Add(variables[i].Guid);
                if (variables[i].Guid == property.stringValue)
                    selectedIndex = i;
            }
        }

        selectedIndex = EditorGUI.Popup(position, selectedIndex, displayValues.ToArray());

        if (referenceValues.Count > 0)
        {
            property.stringValue = referenceValues[selectedIndex];
            return variables[selectedIndex];
        }
        else
        {
            // no variables available, so we clear the reference
            property.stringValue = "";
            return null;
        }
    }

    private string GetVariableDisplayValue(Variable variable)
    {
        string type = "";
        switch (variable.Type)
        {
            case Variable.VariableType.Integer:
                type = "int";
                break;
            case Variable.VariableType.Boolean:
                type = "bool";
                break;
        }
        return string.Format("({0}) {1}", type, variable.Name);
    }

    protected void DropDownField(Rect position, SerializedProperty property, string[] options)
    {
        var selectedIndex = property.enumValueIndex;
        selectedIndex = EditorGUI.Popup(position, selectedIndex, options);
        property.enumValueIndex = selectedIndex;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        //return label != GUIContent.none && Screen.width < 333 ? (16f + 18f) : 16f;
        return 34f;   // 2 lines (16px) and 2px space between lines, also 10px extra space below
    }
}
