﻿namespace Hoerspur.Handles
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;

    public class CustomHandles
    {
        private static int FreeMoveButtonHash = "FreeMoveButton".GetHashCode();

        private static Vector2 FreeMoveButtonMouseStart;
        private static Vector2 FreeMoveButtonMouseCurrent;
        private static Vector2 FreeMoveButtonMouseWorldStart;
        //private static bool FreeMoveButtonDragged;

        public struct FreeMoveButtonResult
        {
            public Vector3 position;
            public bool leftClicked;
            public bool rightClicked;
            public bool dragged;
        }

        public static FreeMoveButtonResult FreeMoveButton(Vector3 position, float handleSize, Handles.CapFunction capFunction)
        {
            int id = GUIUtility.GetControlID(FreeMoveButtonHash, FocusType.Passive);

            Vector3 screenPosition = Handles.matrix.MultiplyPoint(position);
            Matrix4x4 cachedMatrix = Handles.matrix;

            FreeMoveButtonResult result = new FreeMoveButtonResult()
            {
                position = position,
                leftClicked = false,
                rightClicked = false,
                dragged = false
            };

            switch (Event.current.GetTypeForControl(id))
            {
                case EventType.MouseDown:
                    if (HandleUtility.nearestControl == id)
                    {
                        if (Event.current.button == 0)
                        {
                            GUIUtility.hotControl = id;
                            FreeMoveButtonMouseCurrent = FreeMoveButtonMouseStart = Event.current.mousePosition;
                            FreeMoveButtonMouseWorldStart = position;
                            //FreeMoveButtonDragged = false;

                            Event.current.Use();
                            EditorGUIUtility.SetWantsMouseJumping(1);

                            result.position = position;
                            result.leftClicked = true;
                        } else if (Event.current.button == 1)
                        {
                            //Event.current.Use();
                            result.rightClicked = true;
                        }
                    }
                    break;
                case EventType.MouseUp:
                    if (GUIUtility.hotControl == id && (Event.current.button == 0 || Event.current.button == 1))
                    {
                        GUIUtility.hotControl = 0;
                        Event.current.Use();
                        EditorGUIUtility.SetWantsMouseJumping(0);

                        //result.clicked = !FreeMoveButtonDragged;
                        result.position = position;
                        if (result.rightClicked) GUI.changed = true;
                    }
                    break;
                case EventType.MouseDrag:
                    if (GUIUtility.hotControl == id)
                    {
                        FreeMoveButtonMouseCurrent += new Vector2(Event.current.delta.x, -Event.current.delta.y);
                        result.position = Handles.inverseMatrix.MultiplyPoint(Camera.current.ScreenToWorldPoint(Camera.current.WorldToScreenPoint(Handles.matrix.MultiplyPoint(FreeMoveButtonMouseWorldStart)) + (Vector3)(FreeMoveButtonMouseCurrent - FreeMoveButtonMouseStart)));
                        result.dragged = true;
                        GUI.changed = true;
                        //FreeMoveButtonDragged = true;
                        Event.current.Use();
                    }
                    break;
                case EventType.Repaint:
                    Handles.matrix = Matrix4x4.identity;
                    capFunction(id, screenPosition, Quaternion.identity, handleSize, EventType.Repaint);
                    Handles.matrix = cachedMatrix;
                    break;
                case EventType.Layout:
                    Handles.matrix = Matrix4x4.identity;
                    capFunction(id, screenPosition, Quaternion.identity, handleSize, EventType.Layout);
                    Handles.matrix = cachedMatrix;
                    break;
            }

            return result;
        }
    }
}

