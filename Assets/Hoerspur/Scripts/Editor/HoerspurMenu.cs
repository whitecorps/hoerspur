﻿namespace Hoerspur
{
    using Hoerspur.Location;
    using Hoerspur.Variables;
    using Hoerspur.Utils;
    using UnityEditor;
    using UnityEngine;
    using Hoerspur.Audio;
    using Hoerspur.Geometry;
    using UnityEngine.Audio;
    using Hoerspur.Triggers;
    using System;
    using System.IO;

    public class HoerspurMenu : Editor
    {
        [MenuItem("Hoerspur/Create Manager", false, 1)]
        static void AddHoerspurManager()
        {
            var manager = FindOrAddGameObjectWithComponent<HoerspurManager>("HoerspurManager");

            AddVariableManager(manager.gameObject);
            var location = AddLocationManager(manager.gameObject);
            AddAudioManager(manager.gameObject);

            var player = FindOrAddGameObjectWithComponent<Player>("Player");
            player.transform.SetParent(manager.transform);
            location.player = player;
            player.gameObject.SetIcon(IconHelper.LabelIcon.Orange);

            FindOrAddComponent<AudioPlayer>(player.gameObject);

            Selection.activeTransform = manager.transform;
        }

        private static VariableManager AddVariableManager(GameObject parent)
        {
            var variables = FindOrAddComponent<VariableManager>(parent);
            return variables;
        }

        private static LocationManager AddLocationManager(GameObject parent)
        {
            var location = FindOrAddComponent<LocationManager>(parent);
            return location;
        }

        private static AudioManager AddAudioManager(GameObject parent)
        {
            var audio = FindOrAddComponent<AudioManager>(parent);
            var mixer = Resources.Load<AudioMixer>("HoerspurMixer");
            Debug.Log(mixer);
            if (mixer)
            {
                var voiceMixers = mixer.FindMatchingGroups("Voice");
                AudioMixerGroup voiceMixer = null;
                if (voiceMixers.Length > 0)
                {
                    voiceMixer = voiceMixers[0];
                }
                var bgMixers = mixer.FindMatchingGroups("Environment");
                AudioMixerGroup bgMixer = null;
                if (bgMixers.Length > 0)
                {
                    bgMixer = bgMixers[0];
                }
                if (!audio.defaultMixerQueue)
                    audio.defaultMixerQueue = voiceMixer;
                if (!audio.defaultMixerQueueImmediate)
                    audio.defaultMixerQueueImmediate = voiceMixer;
                if (!audio.defaultMixerBackground)
                    audio.defaultMixerBackground = bgMixer;
            }
            return audio;
        }

        [MenuItem("Hoerspur/Create Geometry/Point", false, 3)]
        static void AddNewPointGeometry()
        {
            var manager = FindObjectOfType<HoerspurManager>();
            if (manager == null)
            {
                Debug.LogError("Create Manager first!");
                return;
            }

            Undo.SetCurrentGroupName("Add Point");

            var go = new GameObject("Point");
            go.SetIcon(IconHelper.LabelIcon.Teal);
            Undo.RegisterCreatedObjectUndo(go, "Created Point GameObject");
            //go.transform.SetParent(manager.transform);
            Undo.SetTransformParent(go.transform, manager.transform, "Set Point Parent");

            Undo.AddComponent<PointGeometry>(go);

            Selection.activeObject = go;

            Undo.CollapseUndoOperations(Undo.GetCurrentGroup());
        }

        [MenuItem("Hoerspur/Create Geometry/Polygon", false, 4)]
        static void AddNewPolygonGeometry()
        {
            var manager = FindObjectOfType<HoerspurManager>();
            if (manager == null)
            {
                Debug.LogError("Create Manager first!");
                return;
            }

            Undo.SetCurrentGroupName("Add Polygon");

            var go = new GameObject("Polygon");
            go.SetIcon(IconHelper.LabelIcon.Teal);
            Undo.RegisterCreatedObjectUndo(go, "Created Polygon GameObject");
            //go.transform.SetParent(manager.transform);
            Undo.SetTransformParent(go.transform, manager.transform, "Set Polygon Parent");

            Undo.AddComponent<PolygonGeometry>(go);

            Selection.activeObject = go;

            Undo.CollapseUndoOperations(Undo.GetCurrentGroup());
        }

        [MenuItem("Hoerspur/Save texts to file...", false, 6)]
        static void SaveAllTextsToFile()
        {
            var path = EditorUtility.SaveFilePanel(
                "Save all texts",
                "",
                PlayerSettings.productName + "_texts.txt",
                "txt");

            var triggers = FindObjectsOfType<ATrigger>();
            string contents = "";
            foreach (var trigger in triggers)
            {
                var triggerTexts = trigger.GetTexts();
                if (triggerTexts.Length > 0)
                {
                    var triggerName = trigger.GetName();
                    contents += triggerName + "\n";
                    contents += new String('=', triggerName.Length) + "\n\n";
                    for (int i = 0; i < triggerTexts.Length; i++)
                    {
                        var title = "Target " + (i + 1);
                        contents += title + "\n";
                        contents += new String('-', title.Length) + "\n";
                        contents += triggerTexts[i] + "\n\n";
                    }
                    contents += "\n";
                }
            }

            if (contents.Length > 0)
            {
                File.WriteAllText(path, contents);
                Debug.Log("Texts file created: " + path);
            }
            else
            {
                Debug.LogWarning("Tried to save texts file, but there are no texts to save.");
            }
        }

        static HoerspurManager AddHoerspurManagerComponent()
        {
            var hoerspurManager = FindObjectOfType<HoerspurManager>();
            if (hoerspurManager == null)
            {
                var go = new GameObject("HoerspurManager");
                hoerspurManager = go.AddComponent<HoerspurManager>();
            }
            return hoerspurManager;
        }

        static T FindOrAddGameObjectWithComponent<T>(string name) where T : Component
        {
            T c = FindObjectOfType<T>();
            if (c == null)
            {
                var go = new GameObject(name);
                c = go.AddComponent<T>();
            }
            return c;
        }

        static T FindOrAddComponent<T>(GameObject go) where T : Component
        {
            T c = go.GetComponent<T>();
            if (c == null)
            {
                c = go.AddComponent<T>();
            }
            return c;
        }
    }
}
