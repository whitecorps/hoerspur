﻿namespace Hoerspur
{
    using Hoerspur.GeoJson;
    using Hoerspur.Geometry;
    using Hoerspur.Utils;
    using System.Collections.Generic;
    using System.IO;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(HoerspurManager))]
    public class HoerspurManagerEditor : Editor
    {
        private const string MapTexturePath = "Assets/Hoerspur/Textures/";
        private const string MapTexturePathWithFile = MapTexturePath + "MapTexture.png";

        private const string MapMaterialAssetPath = "Assets/Hoerspur/Materials/MapMaterial.mat";

        private static readonly string[] MapScaleDropdownOptions = { "0.25x", "0.5x", "1x", "2x", "4x", "8x", "16x", "32x" };

        SerializedProperty jsonProperty;
        SerializedProperty centerProperty;
        SerializedProperty mapBoxApiTokenProperty;
        SerializedProperty mapScaleProperty;
        SerializedProperty mapStyleProperty;

        HoerspurManager hoerspurManager;

        bool canEditCenter = false;

        private void OnEnable()
        {
            jsonProperty = serializedObject.FindProperty("geoJson");
            centerProperty = serializedObject.FindProperty("center");
            mapBoxApiTokenProperty = serializedObject.FindProperty("mapBoxApiToken");
            mapScaleProperty = serializedObject.FindProperty("mapScale");
            mapStyleProperty = serializedObject.FindProperty("mapStyle");

            hoerspurManager = target as HoerspurManager;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorHelper.HeaderLabel("JSON File");

            EditorGUILayout.PropertyField(jsonProperty, new GUIContent("JSON File"));

            if (GUILayout.Button("Load file"))
            {
                var jsonFile = jsonProperty.objectReferenceValue as TextAsset;
                var hasFeatureChildren = hoerspurManager.GetComponentsInChildren<AGeometry>().Length > 0;
                if (hasFeatureChildren)
                {
                    // manager already has child elements so we show different options
                    GenericMenu loadMenu = new GenericMenu();
                    loadMenu.AddItem(new GUIContent("Load\u200A\u2215\u200AReplace from file"), false, () =>
                    {
                        if (EditorUtility.DisplayDialog(
                            "Are you sure?",
                            "This will replace all GeoJSON feature objects in Unity.\n\n" +
                            "Any changes to these features will be lost.\n\n" +
                            "The center point of the scene might also change, which could misplace any features you have placed inside of unity.",
                            "Ok",
                            "Cancel"))
                        {
                            BuildSceneFromJson(jsonFile, hoerspurManager.transform, true);
                        }
                    });
                    loadMenu.AddItem(new GUIContent("Load new features only"), false, () =>
                    {
                        BuildSceneFromJson(jsonFile, hoerspurManager.transform, false);
                    });
                    loadMenu.ShowAsContext();
                }
                else
                {
                    // manager has no children, so we just load
                    BuildSceneFromJson(jsonFile, hoerspurManager.transform, true);
                }
            }

            EditorGUILayout.Separator();
            EditorHelper.HeaderLabel("Map Settings");
            EditorGUILayout.PropertyField(mapBoxApiTokenProperty);


            mapScaleProperty.enumValueIndex = EditorGUILayout.Popup("Map Scale", mapScaleProperty.enumValueIndex, MapScaleDropdownOptions);
            EditorGUILayout.PropertyField(mapStyleProperty);
            if (GUILayout.Button("Load map"))
            {
                var mapScale = ((MapScale)mapScaleProperty.enumValueIndex).Value();
                var mapStyle = ((MapStyle)mapStyleProperty.enumValueIndex).Value();
                CreateOrUpdateMapQuad(centerProperty.vector2Value, mapScale, mapStyle);
            }

            EditorGUILayout.Separator();
            EditorHelper.HeaderLabel("Other settings");
            canEditCenter = EditorGUILayout.BeginToggleGroup("Edit center coordinate", canEditCenter);
            EditorGUILayout.PropertyField(centerProperty);
            EditorGUILayout.EndToggleGroup();

            serializedObject.ApplyModifiedProperties();
            SceneView.RepaintAll();
        }

        private void BuildSceneFromJson(TextAsset json, Transform parent, bool replaceExisting)
        {
            FeatureCollection features = FeatureCollection.JsonToFeatureCollection(json);

            if (replaceExisting)
            {
                centerProperty.vector2Value = features.GetBounds().center.YX();
                // ApplyModifiedProperties is called here because there is no way to make sure
                // this function is called from within OnInspectorGUI, where this is usually called
                // could be called from a GenericMenu callback or something
                centerProperty.serializedObject.ApplyModifiedProperties();
            }

            CreateOrUpdateGeometries(features, parent, replaceExisting);
        }

        private void CreateOrUpdateMapQuad(Vector2 center, float scale = 1f, string mapStyle = "")
        {
            // TODO: proper mapping undo
            //var mapTexture = new MapBoxStaticTexture("pk.eyJ1Ijoic21ucGhsciIsImEiOiJjamEyamc4aDI4cjZvMndxcTU0Z2wyMm52In0.GVFvbDXZTrB-50FYnol27g");      // TODO: add field for access token
            var apiToken = mapBoxApiTokenProperty.stringValue;
            var mapTexture = new MapBoxStaticTexture(apiToken);

            mapTexture.StartGetTexture(center, (Texture2D texture) =>
            {
                var textureData = texture.EncodeToPNG();
                if (!Directory.Exists(MapTexturePath))
                {
                    Directory.CreateDirectory(MapTexturePath);
                }
                File.WriteAllBytes(MapTexturePathWithFile, textureData);
                // create/update texture asset
                AssetDatabase.Refresh();
                var storedTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(MapTexturePathWithFile, typeof(Texture2D));

                var quadComponent = hoerspurManager.GetComponentInChildren<MapDisplay>();
                if (quadComponent == null)
                {
                    quadComponent = GameObject.CreatePrimitive(PrimitiveType.Quad).AddComponent<MapDisplay>();
                    quadComponent.gameObject.name = "MapDisplay";
                    var mat = (Material)AssetDatabase.LoadAssetAtPath(MapMaterialAssetPath, typeof(Material));
                    quadComponent.GetComponent<Renderer>().material = mat;
                    quadComponent.transform.SetParent(hoerspurManager.transform);
                }

                var quadScale = WebMercator.Resolution(WebMercator.ZoomForPixelSize(scale));    // calculate exact meters per pixel scale so we can scale quad accordingly
                quadComponent.transform.localScale = Vector3.one * MapBoxStaticTexture.DefaultResolution * quadScale;
                var renderer = quadComponent.GetComponent<Renderer>();
                renderer.sharedMaterial.mainTexture = storedTexture;
            }, metersPerPixel: scale, style: mapStyle);
        }

        private void CreateOrUpdateGeometries(FeatureCollection features, Transform parent, bool replaceExisting)
        {
            Undo.SetCurrentGroupName("Load/update Json file");
            // load a dictionary of already existing geometry components
            var existingGeometries = parent.GetComponentsInChildren<AGeometry>();
            IDictionary<string, AGeometry> existingGeometriesDict = new Dictionary<string, AGeometry>();
            for (int i = 0; i < existingGeometries.Length; i++)
            {
                var existingGeometry = existingGeometries[i];
                existingGeometriesDict.Add(existingGeometry.JsonId, existingGeometry);
            }

            foreach (var feature in features)
            {
                AGeometry geometryComponent;
                GameObject gameObject;
                var geometryExists = existingGeometriesDict.TryGetValue(feature.id, out geometryComponent);
                if (!geometryExists)
                {
                    // create new gameobject
                    gameObject = new GameObject(feature.id);
                    Undo.RegisterCreatedObjectUndo(gameObject, "create " + feature.id);
                    gameObject.transform.SetParent(parent.transform);
                    gameObject.SetIcon(IconHelper.LabelIcon.Green);
                }
                else
                {
                    gameObject = geometryComponent.gameObject;
                }
                if (!geometryExists || (geometryExists && replaceExisting))
                    CreateOrUpdateGeometry(feature, gameObject, centerProperty.vector2Value.YX());
            }
            Undo.CollapseUndoOperations(Undo.GetCurrentGroup());
        }

        private AGeometry CreateOrUpdateGeometry(Feature feature, GameObject gameObject, Vector2 geoRefPoint)
        {
            AGeometry geometry = null;
            switch (feature.geometry.type)
            {
                case "Point":
                    geometry = GetOrAddGeometryComponent<PointGeometry>(gameObject);
                    break;
                case "Polygon":
                    geometry = GetOrAddGeometryComponent<PolygonGeometry>(gameObject);
                    break;
            }
            // set geometry data
            if (geometry != null)
            {
                geometry.LocalCoordinates.Clear();
                var worldRefPoint = WebMercator.LatLonToMeters(geoRefPoint.y, geoRefPoint.x);
                var adjustedWorldCoordinates = new List<Vector2>();
                foreach (var coord in feature.geometry.coordinates)
                {
                    var worldCoord = WebMercator.LatLonToMeters(coord.y, coord.x);
                    adjustedWorldCoordinates.Add((worldCoord - worldRefPoint));
                }
                gameObject.transform.position = GeoLocationHelper.GetBounds(adjustedWorldCoordinates).center;
                geometry.WorldCoordinates = adjustedWorldCoordinates;
                geometry.JsonId = feature.id;
            }
            return geometry;
        }

        private T GetOrAddGeometryComponent<T>(GameObject gameObject) where T : AGeometry
        {
            T geometry = gameObject.GetComponent<T>();
            // if geometry is not the type we want, destroy it and set it to null
            if (geometry != null && !(geometry is T))
            {
                Undo.DestroyObjectImmediate(null);
                geometry = null;
            }
            // if there is no geometry, create the type we want
            if (geometry == null)
            {
                geometry = Undo.AddComponent<T>(gameObject);
            }
            return geometry;
        }

        // for debugging reasons
        private void dumpBounds(Bounds b, string context)
        {
            Debug.Log(context + ": " + b.min.x + ", " + b.min.y + "; " + b.size.x + ", " + b.size.y + "; center: " + b.center.x + ", " + b.center.y);
        }
    }
}