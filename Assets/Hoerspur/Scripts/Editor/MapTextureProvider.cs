﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MapBoxStaticTexture
{
    private const string ApiUrl = "https://api.mapbox.com/{0}?access_token={1}";
    private const string StaticSubUrl = "/styles/v1/mapbox/{0}/static/{1},{2},{3}/{4}x{4}";
    private const string DefaultStyle = "light-v9";
    public const int DefaultResolution = 1024;

    private WWW www;
    private Action<Texture2D> wwwResultOp;
    private string apiToken;


    public MapBoxStaticTexture(string apiToken)
    {
        this.apiToken = apiToken;
    }

    public void StartGetTexture(Vector2 latlng, Action<Texture2D> wwwResultOp, string style = DefaultStyle, float metersPerPixel = 1f, int resolution = DefaultResolution)
    {
        this.wwwResultOp = wwwResultOp;

        var zoom = WebMercator.ZoomForPixelSize(metersPerPixel);
        var requestUrl = BuildRequestUrl(style, latlng, zoom, resolution);
        www = new WWW(requestUrl);

        EditorApplication.update += EditorTick;
    }

    private string BuildRequestUrl(string style, Vector2 latlng, float zoom, int resolution)
    {
        string requestSubUrl = string.Format(StaticSubUrl, style, latlng.y, latlng.x, zoom, resolution);
        string requestUrl = string.Format(ApiUrl, requestSubUrl, apiToken);
        return requestUrl;
    }

    public void EditorTick()
    {
        if (www.isDone)
        {
            EditorApplication.update -= EditorTick;

            if (!String.IsNullOrEmpty(www.error))
            {
                Debug.LogError("Error getting texture: " + www.error);
            }
            else
            {
                if (wwwResultOp != null)
                    wwwResultOp(www.texture);
            }

            www.Dispose();
        }
    }
}
