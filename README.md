## Unity Hörspur Extension

Diese Erweiterung für Unity3D bietet die Möglichkeit, sogenannte Hörspur-Apps zu erstellen.

Aus einer GeoJSON-Datei wird eine Szenenstruktur generiert, die es erlaubt, je nach GPS Position des Mobilgeräts eine (oder mehrere) Audiodateien abzuspielen.
Die Features aus der GeoJSON Datei können dabei sowohl als Audio-Ursprung als auch als Trigger verwendet werden.

Beim Betreten eines Triggers werden je nach voreingestellten Bedingungen:

1. Eine Audiodatei abgespielt
2. Eine beliebige Anzahl Aktionen ausgeführt, die beispielsweise Variablen verändern oder andere Trigger ein- oder ausschalten

---

Informationen zu Installation und Betrieb gibts im [Wiki](https://bitbucket.org/whitecorps/hoerspur/wiki/).